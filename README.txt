Common Resource Grep

Version: 0.3, 20th October, 2013.

An open source COMMON RESOURCE GREP utility to search for name and content
matches within various types of resources including plain files, archives, images,
POM file dependencies, databases and web resources. 

(C) Copyright 2013 Craig Ryan. All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License
(LGPL) version 2.1 which accompanies this distribution, and is 
available at http://www.gnu.org/licenses/lgpl-2.1.html

CRGREP is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Introduction
------------

INSTALL.txt: read this if you download a binary distribution ready to run
BUILD.txt: read this if you download a source distribution to build CRGREP yourself
README.txt (this file): read to discover what CRGREP is and how to use it

CRGREP in 30 seconds
---------------------

Examples best illustrate what you can do with crgrep. 

1. What file names and content do I have buried in my work area matching 'key'

	$ crgrep -r key target
	
	target/misc.zip[misc/nested_monkey.txt]
	target/monkey-pics.txt
	target/monkey-pics.txt:1:A file about happy monkeys.
	target/test-ear.ear[META-INF/MANIFEST.MF]:5:Created-By: Apache monkey
	target/test-ear.ear[app.war][WEB-INF/lib/stuff.jar][META-INF/maven/com.ryanfuse/stuff/pom.xml]:14:  <name>My key module</name>
	
2. What data in my SQLite database matches 'handle'?

	$ crgrep -d -U "jdbc:sqlite:/databases/db.sqlite3" handle '*'      (~/.crgrep defines user/password)
	
	customers: [id,name,status,joined_date,handle]
	customers: 3,Craig,active,2012-10-24 01:05:44,Craig's handle
	tags: 4,handle
	
3. Which of my photo's are tagged with comments of our holiday in Perth?

	$ crgrep Perth pics/*.jpeg
	
	pics/pic1112.jpg: @{JpegComment=Lovely shot of Perth city, just arrived.} 
	pics/pic1113.jpg: @{JpegComment=Breakfast in a Perth cafe} 

4. Does the google home page contain a 'favicon' reference?

	$ crgrep google_favicon http://www.google.com
	
	http://www.google.com:<!doctype html><html itemscope="itemscope" itemtype="http://schema.org/WebPage"><head><meta itemprop="image" content="/images/google_favicon_128.png"><title>Google</title><script>(function(){
		
5. Do I have any maven (POM) dependencies in my project with content matching 'RunWith'?

	$ crgrep -m RunWith pom.xml
	
	C:/Users/Craig/.m2/repository/junit/junit/4.8.2/junit-4.8.2.jar[org/junit/runner/RunWith.class]
	
Data is grep'able whether by file name or content buried in archives, databases tables, meta data in images 
or any other supported resource type. Combinations are also possible (eg follow POM file dependencies 
buried in archives). 

Get the idea?

General Arguments
-----------------

All grep operations involve a similar set of arguments:

	$ crgrep <match criteria> <resource path(s)>

Wildcards may be specified in either <match criteria> or <resource paths>. Command line
expansion of arguments may occur when they contain wildcard arguments match file entries
in the current directory. While this is fine for <resource paths>, the <match criteria> 
argument needs to be passed unchanged to CRGREP. 

To avoid this, use quotes around the match criteria pattern if it contains wildcards:

	$ crgrep 'software*' file.*

	C:\ crgrep "software*" file.*  		(Windows, use double quotes)

To read from standard input (stdin) specify a single hyphen, '-', for <resource path>

	$ cat file.txt | crgrep sometext -

CYGWIN warning: cygwin bash is not currently supported in full as the handling of command line arguments,
and specifically wildcard expansion, varies in behaviour depending on how the script is executed and what
Java runtime is being used. The main issue is with the <match criteria> which needs to be preserved as-is
and passed into CRGREP unchanged. This is difficult to achieve, perhaps with an elaborate combination of
awk scripting and CRGREP source changes to accept quoted arguments within java main(). Until then, the 
suggested approach for Windows is - firstly use the .bat in cmd.exe and if you really need to use a 
cygwin shell then avoid <match criteria> wildcards which will match an existing file in the current directory.

User Preferences
----------------

The file HOME/.crgrep may be used to specify non default configuration settings. 

There is a template in the file 'bin/dot-crgrep'

Recognised properties include

	Property		  Value
	---------------------------------------------------------------------
	user			  user name (for db or other authentication)
	password		  password for authentication
	uri				  resource uri, eg JDBC URL 
	maven.depth		  max depth of search within POM dependency tree
	maven.remoteRepos list of maven repository URLs (format urlRepo1;urlRepo2..)
	maven.localRepo   local repo (if not $HOME/.m2/repository)
	http.proxyHost	  proxy host (if required)
	http.proxyPort	  proxy port

Output Format
-------------

Results are displayed in a format that shows the resource, any nesting information, 
any line numbers and the matching text. Line numbers may not make sense for certain
file types such as matched meta data within images.

The general result form for file based resources is

	<resource>[[:pagenum]:linenum:matching_content]

The <resource> uses specific delimiters to show nestings/groupings as follows

	Sequence   				 			Meaning
	------------------------------------------------------------------
	dir/dir2/file						standard file path
	outer[inner]						inner within a container/archive outer
	outer[mid][inner]					inner within mid within outer
	pom->dependency						an artifact resource within a POM 
										dependency tree
	image: @{metadata}					meta-data within an image file	

	Some concrete <resource> examples:

	Resource path	 					Type
	------------------------------------------------------------------
	src/main/foo.java					normal file path
	lib/res.tar[notes.txt]				.txt file nested in an archive	
	stuff.tar[foo.jar][pom.xml]			a pom.xml within a jar within a tar
	pom.xml->foo.jar					a foo.jar artifact of a pom.xml
	pic.gif: @{ImageResX=45 dots}		meta data within a GIF file
	report.pdf:3:10						page 3, line 10 of a PDF file
	
The general form for database resources is

	<table>: '[' <columns> ']'
	<table>: matching data as CSV

	The <table> and <columns> parts use characters to denote db data:

	Sequence   				 			Meaning
	------------------------------------------------------------------
	{CUSTOMER}: [ID,CREATED,NAME]		Table with matching column names
	{CUSTOMER}: 1,1-1-13,Fred			Column data for a table (as CSV)

Some complete examples of arbitrary crgrep output

	 Output									Match
	 ------------------------------------------------------------------
	 src/foo.java							File listing match
	 src/bar.txt:25:some text				File content match (+lineno)
	 lib/all.zip[image.gif]					Archive file listing match
	 lib/app.war[WEB-INF/web.xml]:6:<d..>   Archive file content match
	 pom.xml->stuff.zip[doc.txt]			File listing match
	 mypic.jpg: @{Size=25,Com=Scene}		File meta-data match
	 {TAB}: [COL1,COL2,COL3]				Table column name match
	 {TAB}: data1,data2,data3				Table data match
     sample.pdf:1:1:Sample PDF Document     Text extracted from a PDF (+pageno and +linenum)

File Grep
---------

The file grep will search for textual matches in the following resource types

* plain text files similar to normal grep
* resources within archives such as ZIP, TAR, WAR, EAR and JAR formats
* meta data in images (jpeg/gif/etc)
* POM files for dependency trees of artifact resources
* text extracted from PDF files

One subtle difference to normal grep is that CRGREP will not only show content matches but also
resources whose name matches the search pattern. The '-l' option simply excludes showing matching
content.

Crgrep search behaviour is according to the file type. 

For plain text file the search is a simple search of the file contents and on file name itselg. 

For archives the files contained within the archive are extracted (in memory) and their contents searched. 
This process may be recursive; if files within an archive are themselves 
archives then these are fist extracted and so on, to any depth. 

For images, the meta data associated with most image files is extracted and then searched for matches.

Maven POM files can be processed and their dependencies (artifacts) included in the search list 
to a specified nesting limit (using the -m option).

PDF files are filtered to extract text content which is then searched for matches.

	 Examples of file searches

		$ crgrep document myreport.pdf
		-> search a single file

		$ crgrep document file1 file2 file3
		-> search specific files

		$ crgrep -r foobah src/test/resources
		-> recursive search (including content) of files under src/test/resources for 'foobah'

		$ crgrep -l foobah src/test/resources/*.ear
		-> search all '.ear' files in src/test/resources for 'foobah', displaying matching files
		   by name but not their content

		$ cat file.txt | crgrep document -
		-> search standard input (stdin)

Database Grep
-------------

Note on database grep: this utility is designed for simple development environments, 
not database systems containing millions of records or extremely large files/archives as
there is no smart caching/paging facility.

Note on Oracle support: an Oracle jdbc driver is NOT shipped with this distribution as I'm 
unclear of the legal and licensing requirements. If you have an Oracle database
you will need to copy your oracle jdbc driver into the lib/ directory of CRGREP so
that CRGREP can find it. This will only cause an issue if you attempt to run
CRGREP with a db uri starting with 'jdbc:ora'.

By specifying a database grep (-d option), the search begins with table and column 
names and (optionally) includes a search of the actual column data values for specific 
column types (text and other non-binary data).

The general form of the resource list is <table pattern>[.<column pattern>].

	 Examples
		$ crgrep -d --user=fred --password=pw -U jdbc:mysql://localhost:3306/db foobah '*'
		-> search all table columns and column data in the db accessible via
		   jdbc:mysql://localhost:3306/db (user 'fred, password 'pw') for 'foobah'

		$ crgrep -d foobah "CUSTOM*"
		-> search the db identified in ~/.crgrep for table names matching 'CUSTOM*' which have
		   'foobah' in the table name and/or one or more column names. No column data is searched.

		$ crgrep -d tubby "*LOGS"
		-> search the db identified in ~/.crgrep for table names matching '*LOGS' which have
		   'tubby' in any column names and/or any column data.

		$ crgrep -d stuff "STAGE*.LOGS*"
		-> search the db identified in ~/.crgrep for table names matching 'STAGE*' which have
		   columns matching 'LOGS*' and which have 'tubby' in the table/column names and 
		   column data (content).

		$ crgrep -d -l "*" "PLAY*.*"
		-> search the db identified in ~/.crgrep and simply list all table names matching 'PLAY*'
		   and matching all column names.

The regular expression format uses standard RE/PERL wildcards. Internally, in the case of 
a database search, the expressions are translated to standard database wildcards as required
by a SQL 'LIKE' type query. Eg 'FOO*' becomes "LIKE 'FOO%'" and so on.

Support for additional database vendors may be added to the project by implementing a 
new crgrep database 'parser' class. A new parser implementation involves following 
two steps:

	. A new <Vendor>Parser class which extends and implements the abstract 
	  methods defined by DatabaseParser.
	. Add a new condition in DatabaseFactory for the vendor specific jdbc URL match

Web Grep
--------

A web page search is conducted when the <resource> begins with 'http://'.

A rather simplistic 'wget' type grep function is supported which will fetch URI pages
and grep the resulting content. Search does not currently extend to links within
the page since no logic exists to limit the depth or identify links already visited.

	 Examples
		$ crgrep google_favicon http://www.google.com
		-> search the web page http://www.google.com HTML for 'google_favicon'

		$ crgrep -u fred -p pw -P myproxy.com:90 google_favicon http://www.google.com
		-> search the web page http://www.google.com HTML for 'google_favicon' via
		   a proxy 'myproxy.com:90' requiring user/password fed/pw.

Trouble shooting
----------------

If crgrep does not function as expected then enabling additional output may help explain the cause.
By default crgrep will report errors only. Warning as disabled, as are debug and trace output.

To enable warnings, specify the verbose option:

	$ crgrep -v ...

To enable debug output (mostly for development), specific using the extensions option

	$ crgrep -X debug ...

To enable both debug and low level trace output specify both in quotes:

	$ crgrep -X "debug,trace" ...

To display other available options specify the help option

    $ crgrep -h	
