/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.util.DisplayRecorder;

/*
 * SQLite DB tests
 * 
 * @author Craig Ryan
 */
public class SqliteDbGrepTest extends TestCommon {

	@Test
	public void testDbContentSqlite() {
		List<String> expected = Arrays.asList(
			"dogooders: 16,Craig,ryan,craig@causefix.com,,active,,craig,,2012-10-24 01:05:44.734375,2012-10-24 01:05:44.734375,Craigo",
			"dogooders: 17,Sara,ryan,sara@causefix.com,,active,,sara,,2012-10-24 01:05:44.906250,2012-10-24 01:05:44.906250,Sarao",
			"dogooders: 18,John,ryan,john@causefix.com,,active,,john,,2012-10-24 01:05:45.000000,2012-10-24 01:05:45.000000,Johno",
			"causes: [id,name,title,content,created_at,updated_at]",
			"comments: [id,commentor,body,cause_id,created_at,updated_at]"
		);
		title("sqlite", "cause", "*");
		String[] args = new String[] {
				"-d",
				//"-X", "debug", 
				"-v",
				"-U", "jdbc:sqlite:" + projectDir() + "/src/test/databases/development.sqlite3",
				"cause", "*"
		};
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		display.setSortedPaths(false);
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(ListUtils.isEqualList(expected, actual));
	}
}
