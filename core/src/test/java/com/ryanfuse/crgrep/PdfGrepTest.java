/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * Test for matches against text data extracted from PDF files.
 * 
 * @author Craig Ryan
 */
public class PdfGrepTest extends TestCommon {

	@Test
	public void testPdf() {
		List<String> expected = Arrays.asList(
			//"src/test/resources/pdf/sample.pdf:1:10:Document"
				"src/test/resources/pdf/sample.pdf:1:1:Sample PDF Document",
				"src/test/resources/pdf/sample.pdf:3:6:1.2 How to write a document . . . . . . . . . . . . . . . . . . . . . . 6",
				"src/test/resources/pdf/sample.pdf:3:7:1.2.1 The main document . . . . . . . . . . . . . . . . . . . . . 6",
				"src/test/resources/pdf/sample.pdf:5:11:� efax package could be useful, if you plan to fax documents.",
				"src/test/resources/pdf/sample.pdf:5:16:2. run �pdflatex file.tex� on the main file of the document three times",
				"src/test/resources/pdf/sample.pdf:6:8:1.2 How to write a document",
				"src/test/resources/pdf/sample.pdf:6:9:1.2.1 The main document",
				"src/test/resources/pdf/sample.pdf:6:10:Choose the name of the document, say document. Copy template.tex to",
				"src/test/resources/pdf/sample.pdf:6:11:document.tex, then edit it, change the title, the authors and set proper in-",
				"src/test/resources/pdf/sample.pdf:6:14:Each chapter should be included in the main document as a separate file. You can",
				"src/test/resources/pdf/sample.pdf:6:16:main file. For our example we use the file name document_chapter1.tex.",
				"src/test/resources/pdf/sample.pdf:6:17:First, copy template_chapter.tex to document_chapter1.tex",
				"src/test/resources/pdf/sample.pdf:6:19:/include{document_chapter1}",
				"src/test/resources/pdf/sample.pdf:6:20:in the document.tex, then edit document_chapter1.tex, change the",
				"src/test/resources/pdf/sample.pdf:6:28:ispell -t document_chapter1.tex",
				"src/test/resources/pdf/sample.pdf:7:15:A lot of examples can be found in this document.",
				"src/test/resources/pdf/sample.pdf:8:19:There are other ways to do this, see the documentation for inputenc pack-"
		);
		title("pdf sample", "ocument", "src/test/resources/pdf/sample.pdf");
		String[] args = new String[] { /*"-X", "debug,trace",*/ "ocument", "src/test/resources/pdf/sample.pdf" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		display.setSortedPaths(false); // pdf lines are searched in order. Dont sort output
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}
}
