/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.List;

import org.apache.commons.collections.ListUtils;

import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * Common test methods to display test output and identify resource list differences.
 * 
 * @author Craig Ryan
 */
public class TestCommon {
	
	protected DisplayRecorder display;
	
	protected void title(String test, String pat, String res) {
		System.out.println("------------------- " + test + " pattern: [" + pat + "] resource: [" + res + "]");
	}

	// 'pwd' for crgrep project
	public static String projectDir() {
		return System.getProperty("user.dir").replaceAll("\\\\", "/");
	}
	
	public static String getUserHome() {
		return System.getProperty("user.home").replaceAll("\\\\", "/");
	}
	
	/*
	 * Print out the actual list in a readable format and any differences
	 * between actual/expected lists.
	 */
	public String actualList(List<String> res, List<String> expected) {
		StringBuffer sb = new StringBuffer("Unexpected result, actual list entries:\n");
		for (String l : res) {
			sb.append(l).append("\n");
		}
		List<String> diff;
		sb.append("-----------------------------------------------------------------");
		if ((expected != null) && (res.size() != expected.size())) {
			sb.append("Actual size [" + res.size() + "]  expected size [" + expected.size() + "]\n");
			if (res.containsAll(expected)) { 
				// too many actual results
				sb.append("Too many actual results, extra entries are:\n");
				diff = ListUtils.subtract(res, expected);
			} else {
				// too few results
				sb.append("Too few actual results, extra expected entries are:\n");
				diff = ListUtils.subtract(expected, res);
			}
		} else {
			sb.append("Actual == expected size, contents must be different! Entries not matching:\n");
			diff = ListUtils.subtract(expected, res);
		}
		if (diff == null || diff.isEmpty()) {
			sb.append("<empty list>\n");
		} else {
			for (String l : diff) {
				sb.append(l).append("\n");
			}
		}
		return sb.toString();
	}
}
