/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * Web page (HTTP GET) tests.
 * 
 * If this test fails due to assert failure, it is most likely google have changed the html around the favicon text.
 * 
 * @author Craig Ryan
 */
public class HttpGrepTest extends TestCommon {

	@Test
	public void testHttp() {
		title("http", "google_favicon", "http://www.google.com");
		List<String> expected = Arrays.asList(
			"http://www.google.com:<!doctype html><html itemscope=\"\" itemtype=\"http://schema.org/WebPage\"><head><meta itemprop=\"image\" content=\"/images/google_favicon_128.png\"><title>Google</title><script>(function(){"				
		);
		String[] args = new String[] {
			"google_favicon", "http://www.google.com" 
		};
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
		        ListUtils.subtract(expected, actual).toString(),
		        ListUtils.isEqualList(expected, actual));
	}
}
