/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * Test for matches against specific meta data stored within an image file. 
 * 
 * @author Craig Ryan
 */
public class ImageGrepTest extends TestCommon {

	@Test
	public void testImageEasyShooting() {
		List<String> expected = Arrays.asList(
				"src/test/resources/images/Canon IXUS v3.jpg: @{ExposureMode=Easy shooting}",
				"src/test/resources/images/Canon PowerShot S300.jpg: @{ExposureMode=Easy shooting}"
				);
		title("images easy shooting", "hoo", "src/test/resources/images/Can*");
		String[] args = new String[] { "-r", /*"-X", "debug,trace",*/ "hoo", "src/test/resources/images/Can*" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testImageDPI() {
		List<String> expected = Arrays.asList(
			"src/test/resources/images/Canon EOS D60.jpg: @{ResolutionInfo=300.0x300.0 DPI}",
			"src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{ResolutionInfo=300.0x300.0 DPI}"
			);
		title("images DPI", "DPI", "src/test/resources/images");
		String[] args = new String[] { "-r", /*"-X", "debug",*/ "DPI", "src/test/resources/images" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testImageDotsPerInch() {
		List<String> expected = Arrays.asList(
			"src/test/resources/images/Apple iPhone 4.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Canon EOS D60.jpg: @{XResolution=300 dots per inch, YResolution=300 dots per inch}",
			"src/test/resources/images/Canon EOS D60.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Canon IXUS v3.jpg: @{XResolution=180 dots per inch, YResolution=180 dots per inch}",
			"src/test/resources/images/Canon PowerShot S300.jpg: @{XResolution=180 dots per inch, YResolution=180 dots per inch}",
			"src/test/resources/images/Epson PhotoPC 3000Z.jpg: @{XResolution=480 dots per inch, YResolution=480 dots per inch}",
			"src/test/resources/images/Epson PhotoPC 3000Z.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/FujiFilm DS-7 (2).jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{XResolution=300 dots per inch, YResolution=300 dots per inch}",
			"src/test/resources/images/FujiFilm FinePixS2Pro.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/HP PhotoSmart 735.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Kodak DC210.jpg: @{XResolution=216 dots per inch, YResolution=216 dots per inch}",
			"src/test/resources/images/Kodak DC210.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Minolta DiMAGE X.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Nikon D1X.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Nikon E5000.jpg: @{XResolution=300 dots per inch, YResolution=300 dots per inch}",
			"src/test/resources/images/Olympus C4040Z.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Pentax Optio S4.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Ricoh DC-3Z (low res).jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Sanyo SR6.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Sony Cybershot (3).jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}",
			"src/test/resources/images/Sony DigitalMavica.jpg: @{XResolution=72 dots per inch, YResolution=72 dots per inch}"
			);
		title("images dots per inch", "dots per inch", "src/test/resources/images");
		String[] args = new String[] { "-r", /*"-X", "debug,trace",*/ "dots per inch", "src/test/resources/images" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testImageDotsPerInchNoContent() {
		List<String> expected = Arrays.asList(
			"src/test/resources/images/Apple iPhone 4.jpg",
			"src/test/resources/images/Canon EOS D60.jpg",
			"src/test/resources/images/Canon IXUS v3.jpg",
			"src/test/resources/images/Canon PowerShot S300.jpg",
			"src/test/resources/images/Epson PhotoPC 3000Z.jpg",
			"src/test/resources/images/FujiFilm DS-7 (2).jpg",
			"src/test/resources/images/FujiFilm FinePixS2Pro.jpg",
			"src/test/resources/images/HP PhotoSmart 735.jpg",
			"src/test/resources/images/Kodak DC210.jpg",
			"src/test/resources/images/Minolta DiMAGE X.jpg",
			"src/test/resources/images/Nikon D1X.jpg",
			"src/test/resources/images/Nikon E5000.jpg",
			"src/test/resources/images/Olympus C4040Z.jpg",
			"src/test/resources/images/Pentax Optio S4.jpg",
			"src/test/resources/images/Ricoh DC-3Z (low res).jpg",
			"src/test/resources/images/Sanyo SR6.jpg",
			"src/test/resources/images/Sony Cybershot (3).jpg",
			"src/test/resources/images/Sony DigitalMavica.jpg"
			);
		title("images dots per inch no content", "dots", "src/test/resources/images");
		String[] args = new String[] { "-r", "-l", /*"-X", "debug",*/ "dots", "src/test/resources/images" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testImageUserComment() {
		List<String> expected = Arrays.asList(
				"src/test/resources/images/Kodak DC210.jpg: @{JpegComment=Lovely smoke effect here, of which I'm secretly very proud.}"
				);
		title("images user comment", "smoke", "src/test/resources/images");
		String[] args = new String[] { "-r", /*"-X", "debug",*/ "smoke", "src/test/resources/images" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}
}
