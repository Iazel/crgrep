/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

/**
 * Display wrapper which records results displayed, to aid test result comparisons.
 * 
 * Path conversion to Unix separators combined with sorting of paths (rather 
 * than storing them in chronological order) makes for platform independent
 * testing.
 * 
 * @author Craig Ryan
 */
public class DisplayRecorder extends Display {

	private List<String> msgs = new ArrayList<String>();
	private Display display;
	// By default all logged results are converted to Unix path separators
    private boolean unixPaths = true;
    // Whether to sort the list of paths
    private boolean sortedPaths = true;
	
	public DisplayRecorder(Display display) {
		this.display = display;
	}

	@Override
	public Display result(String msg) {
		String logMsg = msg;
	    if (unixPaths) {
	    	if (display.getPrefix() != null) {
	    		logMsg = display.getPrefix() + msg;
	    	}
	        msg = FilenameUtils.separatorsToUnix(msg);
	        logMsg = FilenameUtils.separatorsToUnix(logMsg);
	    }
		msgs.add(logMsg);
		return display.result(msg);
	}
	
    public List<String> messages() {
	    if (sortedPaths) {
	        Collections.sort(msgs);
	    }
		return msgs;
	}

    /**
     * Determines if results are converted to 
     * @param unixPaths
     */
    public void setUnixPaths(boolean unixPaths) {
        this.unixPaths = unixPaths;
    }

    public void setSortedPaths(boolean sortedPaths) {
        this.sortedPaths = sortedPaths;
    }

	public Display warn(String msg) {
		return display.warn(msg);
	}

	public Display error(String msg) {
		return display.error(msg);
	}

	public Display trace(String msg) {
		return display.trace(msg);
	}

	public Display debug(String msg) {
		return display.debug(msg);
	}

	public String getPrefix() {
        return display.getPrefix();
    }

    public void unsetPrefix() {
        display.unsetPrefix();
    }

    public void setPrefix(String prefix) {
        display.setPrefix(prefix);
    }

    public String getLogPrefix() {
		return display.getLogPrefix();
	}

	public void setLogPrefix(String prefix) {
		display.setLogPrefix(prefix);
	}

	public boolean isDebugOn() {
		return display.isDebugOn();
	}

	public void setDebugOn(boolean debugOn) {
		display.setDebugOn(debugOn);
	}

	public boolean isTraceOn() {
		return display.isTraceOn();
	}

	public void setTraceOn(boolean traceOn) {
		display.setTraceOn(traceOn);
	}
}
