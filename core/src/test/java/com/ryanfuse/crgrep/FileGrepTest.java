/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * All File type grep tests
 * 
 * @author Craig Ryan
 */
public class FileGrepTest extends TestCommon {

	private DisplayRecorder display;

	@Test
	public void testTarGzip() {
		List<String> expected = Arrays.asList(
			"target/resources/resources.tar.gz[monkey-pics.txt]",
			"target/resources/resources.tar.gz[monkey-pics.txt]:1:A file about happy monkeys.",
			"target/resources/resources.tar.gz[monkey-pics.txt]:3:A monkey was once here.",
			"target/resources/resources.tar.gz[monkey-pics.txt]:5:End of my story.",
			"target/resources/resources.tar.gz[some-other-monkey.txt]",
			"target/resources/resources.tar.gz[some-other-monkey.txt]:1:Another monkey here. ",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/]",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/]",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/]",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.properties]",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.properties]:4:groupId=com.ryanfuse",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:19:		  <groupId>org.apache.maven.plugins</groupId>",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:1:<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:20:		  <artifactId>maven-jar-plugin</artifactId>",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:2:<project xmlns=\"http://maven.apache.org/POM/4.0.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:34:	  	  <scope>runtime</scope>",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:3:  	<modelVersion>4.0.0</modelVersion>",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:6:        <groupId>com.ryanfuse</groupId>",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:9:        <relativePath>../pom.xml</relativePath>"
		);
		title("file content", "m", "target/resources/r*.tar.gz");
		String[] args = new String[] { "-r", /*"-X", "debug",*/ "m", "target/resources/r*.tar.gz" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
		        ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testFilesNoContent() {
		List<String> expected = Arrays.asList(
			"target/resources/misc.zip[misc/nested_monkey.txt]",
            "target/resources/misc/nested_monkey.txt",
			"target/resources/monkey-pics.txt",
			"target/resources/monkey-resources.7z",
			"target/resources/resources.tar.gz[monkey-pics.txt]",
			"target/resources/resources.tar.gz[some-other-monkey.txt]",
            "target/resources/resources.tar[monkey-pics.txt]",
            "target/resources/resources.tar[some-other-monkey.txt]",
			"target/resources/resources.zip[monkey-pics.txt]",
			"target/resources/resources.zip[some-other-monkey.txt]",
			"target/resources/some-other-monkey.txt",
			"target/resources/test-war-0.1.war[WEB-INF/classes/images/capture_monkey.PNG]",
			"target/resources/test-war-0.1.war[monkey_play.html]"
			);
		
		title("file recurse, no content", "monkey", "target/resources");
		String[] args = new String[] { "-r", "-l", /*"-X", "debug",*/ "monkey", "target/resources" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testEarFilesContent() {
		List<String> expected = Arrays.asList(
			"target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:2:WebLogic-Application-Version: Monkey1.0",
			"target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:5:Created-By: Apache monkey",
			"target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:14:  	<name>Test Ear of the Monkey</name>",
			"target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:27:				<WebLogic-Application-Version>Monkey1.0</WebLogic-Application-Version>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][META-INF/maven/com.ryanfuse/test-war/pom.xml]:14:  	<name>Test War of the Monkeys</name>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/classes/images/capture_monkey.PNG]",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/web.xml]:6:  <display-name>HelloMonkey Application</display-name>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][index.html]:2:Hi monkey",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]:2:	<head>stuff of monkeys</head>"
		);

		title("ear file content", "onkey", "target/resources/*.ear");
		String[] args = new String[] { "-r", /*"-v", "-X", "debug",*/ "onkey", "target/resources/*.ear" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testRecursiveContent() {
		List<String> expected = Arrays.asList(
			"target/resources/junit-pom.xml:10:		<monkey-junit.version>4.8.2</monkey-junit.version>",
			"target/resources/junit-pom.xml:20:			<version>${monkey-junit.version}</version>",
			"target/resources/misc.zip[misc/nested_monkey.txt]",
			"target/resources/misc.zip[misc/nested_monkey.txt]:1:A file nested under misc which mentions a monkey",
			"target/resources/misc.zip[misc/nested_monkey.txt]:4:but a monkey indeed",
			"target/resources/misc/nested_monkey.txt",
			"target/resources/misc/nested_monkey.txt:1:A file nested under misc which mentions a monkey",
			"target/resources/misc/nested_monkey.txt:4:but a monkey indeed",
			"target/resources/monkey-pics.txt",
			"target/resources/monkey-pics.txt:1:A file about happy monkeys.",
			"target/resources/monkey-pics.txt:3:A monkey was once here.",
			"target/resources/monkey-resources.7z",
			"target/resources/resources.tar.gz[monkey-pics.txt]",
			"target/resources/resources.tar.gz[monkey-pics.txt]:1:A file about happy monkeys.",
			"target/resources/resources.tar.gz[monkey-pics.txt]:3:A monkey was once here.",
			"target/resources/resources.tar.gz[some-other-monkey.txt]",
			"target/resources/resources.tar.gz[some-other-monkey.txt]:1:Another monkey here. ", 
			"target/resources/resources.tar.gz[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
			"target/resources/resources.tar.gz[test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
			"target/resources/resources.tar[monkey-pics.txt]",
			"target/resources/resources.tar[monkey-pics.txt]:1:A file about happy monkeys.",
			"target/resources/resources.tar[monkey-pics.txt]:3:A monkey was once here.",
			"target/resources/resources.tar[some-other-monkey.txt]",
			"target/resources/resources.tar[some-other-monkey.txt]:1:Another monkey here. ",
			"target/resources/resources.tar[test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
			"target/resources/resources.tar[test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
			"target/resources/resources.zip[monkey-pics.txt]",
			"target/resources/resources.zip[monkey-pics.txt]:1:A file about happy monkeys.",
			"target/resources/resources.zip[monkey-pics.txt]:3:A monkey was once here.",
			"target/resources/resources.zip[some-other-monkey.txt]",
			"target/resources/resources.zip[some-other-monkey.txt]:1:Another monkey here. ",
			"target/resources/some-other-monkey.txt",
			"target/resources/some-other-monkey.txt:1:Another monkey here. ",
			"target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:2:WebLogic-Application-Version: Monkey1.0",
			"target/resources/test-ear-0.1.ear[META-INF/MANIFEST.MF]:5:Created-By: Apache monkey",
			"target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:14:  	<name>Test Ear of the Monkey</name>",
			"target/resources/test-ear-0.1.ear[META-INF/maven/com.ryanfuse/test-ear/pom.xml]:27:				<WebLogic-Application-Version>Monkey1.0</WebLogic-Application-Version>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][META-INF/maven/com.ryanfuse/test-war/pom.xml]:14:  	<name>Test War of the Monkeys</name>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/classes/images/capture_monkey.PNG]",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/lib/test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][WEB-INF/web.xml]:6:  <display-name>HelloMonkey Application</display-name>",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][index.html]:2:Hi monkey",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]",
			"target/resources/test-ear-0.1.ear[test-war-0.1.war][monkey_play.html]:2:	<head>stuff of monkeys</head>",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
			"target/resources/test-jar-0.1.jar[org/crgrep/TestMonkey.class]",
			"target/resources/test-war-0.1.war[META-INF/maven/com.ryanfuse/test-war/pom.xml]:14:  	<name>Test War of the Monkeys</name>",
			"target/resources/test-war-0.1.war[WEB-INF/classes/images/capture_monkey.PNG]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]:14:  	<name>Test Jar in Monkey hands</name>",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][org/crgrep/TestMonkey.class]",
			"target/resources/test-war-0.1.war[WEB-INF/web.xml]:6:  <display-name>HelloMonkey Application</display-name>",
			"target/resources/test-war-0.1.war[index.html]:2:Hi monkey",
			"target/resources/test-war-0.1.war[monkey_play.html]",
			"target/resources/test-war-0.1.war[monkey_play.html]:2:	<head>stuff of monkeys</head>"
		);

		title("file recursive content", "onkey", "target/resources/");
		String[] args = new String[] { "-r", /*"-v", "-X", "debug",*/ "onkey", "target/resources/" };
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}
	
	@Test
	public void testStandardInput() {
		List<String> expected = Arrays.asList(
				"<stdin>:1:A file about happy monkeys.",
				"<stdin>:3:A monkey was once here."
			);

			title("stdin content", "monkey", "-");
			String[] args = new String[] { /*"-v", "-X", "debug",*/ "monkey", "-" };

			File infile = new File("target/resources/monkey-pics.txt");
			FileInputStream fis = null;
			try {
				fis = new FileInputStream(infile);
			} catch (FileNotFoundException e) {
				fail("Failed to read file target/resources/monkey-pics.txt");
			}
			// Re-assign stdin to our test data file
			System.setIn(fis);
			
			ResourceGrep rg = new ResourceGrep(args);
			display = new DisplayRecorder(rg.getDisplay());
			rg.setDisplay(display);
			rg.execute();
			List<String> actual = display.messages();
			assertTrue(
					actualList(actual, expected),
					ListUtils.isEqualList(expected, actual));
	}
}
