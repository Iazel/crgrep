/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Integration test for all maven POM tests
 * 
 * @author Craig Ryan
 */
public class PomFileGrepTest extends TestCommon {

	private DisplayRecorder display;

	@Test
	public void testPomFileNoContent() {
		title("POM file no content", "junit", "target/resources/junit-pom.xml");
        String[] args = new String[] { "-m", "-l", "junit", "target/resources/junit-pom.xml" };
		ResourceGrep rg = new ResourceGrep(args);
		String repoDir = getRepoBaseDir(rg.getSwitches());
		List<String> expected = Arrays.asList(
				"target/resources/junit-pom.xml",
				"target/resources/junit-pom.xml->" + repoDir + "junit/junit/4.8.2/junit-4.8.2.jar"
	    );
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
				ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testPomFileFollowDependencies() {
		title("POM file content", "RunWith", "target/resources/*pom.xml");
        String[] args = new String[] { "-m", /*"-X", "debug",*/ "RunWith", "target/resources/*pom.xml" };
		ResourceGrep rg = new ResourceGrep(args);
		String repoDir = getRepoBaseDir(rg.getSwitches());
		List<String> expected = Arrays.asList(
				"target/resources/junit-pom.xml->" + repoDir + "junit/junit/4.8.2/junit-4.8.2.jar[org/junit/runner/RunWith.class]");
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
			actualList(actual, expected),
			ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testPomFileFollowDepsTextFileMatch() {
		title("POM file content", "All rights reserved", "trget/resources/*pom.xml");
        String[] args = new String[] { "-m", "All rights reserved", "target/resources/*pom.xml" };
		ResourceGrep rg = new ResourceGrep(args);
		String repoDir = getRepoBaseDir(rg.getSwitches());
		List<String> expected = Arrays.asList(
				"target/resources/junit-pom.xml->" + repoDir + "junit/junit/4.8.2/junit-4.8.2.jar[LICENSE.txt]:4:All rights reserved.");

		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
				actualList(actual, expected),
			ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testPomFileInJarArchive() {
		title("POM within jar archive content", "ize", "target/resources/test-jar-*.jar");
        String[] args = new String[] { "-m", /*"-X", "debug",*/ "ize", "target/resources/test-jar-*.jar" };
		ResourceGrep rg = new ResourceGrep(args);
		String repoDir = getRepoBaseDir(rg.getSwitches());
		List<String> expected = Arrays.asList(
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "javax/validation/validation-api/1.1.0.Final/validation-api-1.1.0.Final.jar[javax/validation/constraints/Size$List.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "javax/validation/validation-api/1.1.0.Final/validation-api-1.1.0.Final.jar[javax/validation/constraints/Size.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "joda-time/joda-time/2.1/joda-time-2.1.jar[META-INF/LICENSE.txt]:13:      \"Licensor\" shall mean the copyright owner or entity authorized by",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "joda-time/joda-time/2.1/joda-time-2.1.jar[META-INF/LICENSE.txt]:53:      or by an individual or Legal Entity authorized to submit on behalf of",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "joda-time/joda-time/2.1/joda-time-2.1.jar[org/joda/time/tz/data/America/Belize]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages.properties]:14:javax.validation.constraints.Size.message        = size must be between {min} and {max}",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages_cs.properties]:13:javax.validation.constraints.Size.message        = velikost mus/u00ed b/u00fdt mezi {min} a {max}",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages_de.properties]:13:javax.validation.constraints.Size.message        = muss zwischen {min} und {max} liegen",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages_es.properties]:13:javax.validation.constraints.Size.message        = el tama/u00F1o tiene que estar entre {min} y {max}",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages_fr.properties]:13:javax.validation.constraints.Size.message        = la taille doit /u00EAtre entre {min} et {max}",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages_hu.properties]:13:javax.validation.constraints.Size.message        = a m/u00E9retnek {min} /u00E9s {max} k/u00F6z/u00F6tt kell lennie",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages_hu.properties]:5:javax.validation.constraints.Digits.message      = a sz/u00E1mform/u00E1tum nem felel meg a k/u00F6vetkez/u0151 form/u00E1nak/: <{integer} sz/u00E1mjegy>.<{fraction} tizedesjegy>",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages_mn_MN.properties]:13:javax.validation.constraints.Size.message        = /u0425/u044D/u043C/u0436/u044D/u044D {min}-/u0441 {max} /u0445/u043E/u043E/u0440/u043E/u043D/u0434 /u0431/u0430/u0439/u043D/u0430",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages_pt_BR.properties]:14:javax.validation.constraints.Size.message        = tamanho deve estar entre {min} e {max}",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages_tr.properties]:13:javax.validation.constraints.Size.message        = boyut '{min}' ile '{max}' aras/u0131nda olmal/u0131",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/ValidationMessages_zh_CN.properties]:13:javax.validation.constraints.Size.message        = /u4E2A/u6570/u5FC5/u987B/u5728{min}/u548C{max}/u4E4B/u95F4",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/cfg/defs/SizeDef.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForArray.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForArraysOfBoolean.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForArraysOfByte.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForArraysOfChar.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForArraysOfDouble.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForArraysOfFloat.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForArraysOfInt.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForArraysOfLong.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForArraysOfPrimitives.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForArraysOfShort.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForCharSequence.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForCollection.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/constraintvalidators/SizeValidatorForMap.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/engine/messageinterpolation/LocalizedMessage.class]",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/util/logging/Log.i18n.properties]:140:# Message: Unable to initialize %s.",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/util/logging/Log.i18n.properties]:142:getUnableToInitializeConstraintValidatorException=Unable to initialize %s.",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/util/logging/Log.i18n.properties]:249:# Message: Method %1$s has %2$s parameters, but the passed list of parameter meta data has a size of %3$s.",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/util/logging/Log.i18n.properties]:252:# @param 3: listSize - ",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[org/hibernate/validator/internal/util/logging/Log.i18n.properties]:253:getInvalidLengthOfParameterMetaDataListException=Method %1$s has %2$s parameters, but the passed list of parameter meta data has a size of %3$s.",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/jboss/logging/jboss-logging/3.1.1.GA/jboss-logging-3.1.1.GA.jar[META-INF/LICENSE.txt]:120:other authorized party saying it may be distributed under the terms of",
			"target/resources/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/jboss/logging/jboss-logging/3.1.1.GA/jboss-logging-3.1.1.GA.jar[org/jboss/logging/SerializedLogger.class]"
		);

		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
			actualList(actual, expected),
			ListUtils.isEqualList(expected, actual));
	}

	@Test
	public void testPomFileInWarArchive() {
		title("POM within war archive content", "pom", "target/resources/test-war-*.war");
		String[] args = new String[] { "-m", /*"-X", "debug",*/ "pom", "target/resources/test-war-*.war" };
		ResourceGrep rg = new ResourceGrep(args);
		String repoDir = getRepoBaseDir(rg.getSwitches()); 
		List<String> expected = Arrays.asList(
			"target/resources/test-war-0.1.war[META-INF/maven/com.ryanfuse/test-war/pom.properties]",
			"target/resources/test-war-0.1.war[META-INF/maven/com.ryanfuse/test-war/pom.xml]",
			"target/resources/test-war-0.1.war[META-INF/maven/com.ryanfuse/test-war/pom.xml]->" + repoDir + "com/ryanfuse/test-jar/0.1/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.properties]",
			"target/resources/test-war-0.1.war[META-INF/maven/com.ryanfuse/test-war/pom.xml]->" + repoDir + "com/ryanfuse/test-jar/0.1/test-jar-0.1.jar[META-INF/maven/com.ryanfuse/test-jar/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/classmate-0.8.0.jar][META-INF/maven/com.fasterxml/classmate/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/classmate-0.8.0.jar][META-INF/maven/com.fasterxml/classmate/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/hibernate-validator-5.0.1.Final.jar][META-INF/maven/org.hibernate/hibernate-validator/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/hibernate-validator-5.0.1.Final.jar][META-INF/maven/org.hibernate/hibernate-validator/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]->" + repoDir + "log4j/log4j/1.2.16/log4j-1.2.16.jar[META-INF/maven/log4j/log4j/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]->" + repoDir + "log4j/log4j/1.2.16/log4j-1.2.16.jar[META-INF/maven/log4j/log4j/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]->" + repoDir + "org/apache/geronimo/specs/geronimo-jms_1.1_spec/1.0/geronimo-jms_1.1_spec-1.0.jar[META-INF/maven/org.apache.geronimo.specs/geronimo-jms_1.1_spec/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]->" + repoDir + "org/apache/geronimo/specs/geronimo-jms_1.1_spec/1.0/geronimo-jms_1.1_spec-1.0.jar[META-INF/maven/org.apache.geronimo.specs/geronimo-jms_1.1_spec/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]->" + repoDir + "org/jboss/logmanager/jboss-logmanager/1.2.0.GA/jboss-logmanager-1.2.0.GA.jar[META-INF/MANIFEST.MF]:18:Scm-Url: scm:git:git@github.com:jboss/jboss-parent-pom.git/jboss-logma",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]->" + repoDir + "org/jboss/logmanager/jboss-logmanager/1.2.0.GA/jboss-logmanager-1.2.0.GA.jar[META-INF/maven/org.jboss.logmanager/jboss-logmanager/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]->" + repoDir + "org/jboss/logmanager/jboss-logmanager/1.2.0.GA/jboss-logmanager-1.2.0.GA.jar[META-INF/maven/org.jboss.logmanager/jboss-logmanager/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]->" + repoDir + "org/slf4j/slf4j-api/1.6.1/slf4j-api-1.6.1.jar[META-INF/maven/org.slf4j/slf4j-api/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/jboss-logging-3.1.1.GA.jar][META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]->" + repoDir + "org/slf4j/slf4j-api/1.6.1/slf4j-api-1.6.1.jar[META-INF/maven/org.slf4j/slf4j-api/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "com/fasterxml/classmate/0.8.0/classmate-0.8.0.jar[META-INF/maven/com.fasterxml/classmate/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "com/fasterxml/classmate/0.8.0/classmate-0.8.0.jar[META-INF/maven/com.fasterxml/classmate/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "javax/validation/validation-api/1.1.0.Final/validation-api-1.1.0.Final.jar[META-INF/maven/javax.validation/validation-api/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "javax/validation/validation-api/1.1.0.Final/validation-api-1.1.0.Final.jar[META-INF/maven/javax.validation/validation-api/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "joda-time/joda-time/2.1/joda-time-2.1.jar[META-INF/maven/joda-time/joda-time/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "joda-time/joda-time/2.1/joda-time-2.1.jar[META-INF/maven/joda-time/joda-time/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[META-INF/maven/org.hibernate/hibernate-validator/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/hibernate/hibernate-validator/5.0.1.Final/hibernate-validator-5.0.1.Final.jar[META-INF/maven/org.hibernate/hibernate-validator/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/jboss/logging/jboss-logging/3.1.1.GA/jboss-logging-3.1.1.GA.jar[META-INF/maven/org.jboss.logging/jboss-logging/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/jboss/logging/jboss-logging/3.1.1.GA/jboss-logging-3.1.1.GA.jar[META-INF/maven/org.jboss.logging/jboss-logging/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/jsoup/jsoup/1.7.1/jsoup-1.7.1.jar[META-INF/maven/org.jsoup/jsoup/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/test-jar-0.1.jar][META-INF/maven/com.ryanfuse/test-jar/pom.xml]->" + repoDir + "org/jsoup/jsoup/1.7.1/jsoup-1.7.1.jar[META-INF/maven/org.jsoup/jsoup/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/validation-api-1.1.0.Final.jar][META-INF/maven/javax.validation/validation-api/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/validation-api-1.1.0.Final.jar][META-INF/maven/javax.validation/validation-api/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/validation-api-1.1.0.Final.jar][META-INF/maven/javax.validation/validation-api/pom.xml]->" + repoDir + "com/beust/jcommander/1.27/jcommander-1.27.jar[META-INF/maven/com.beust/jcommander/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/validation-api-1.1.0.Final.jar][META-INF/maven/javax.validation/validation-api/pom.xml]->" + repoDir + "com/beust/jcommander/1.27/jcommander-1.27.jar[META-INF/maven/com.beust/jcommander/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/validation-api-1.1.0.Final.jar][META-INF/maven/javax.validation/validation-api/pom.xml]->" + repoDir + "org/hamcrest/hamcrest-core/1.1/hamcrest-core-1.1.jar[META-INF/maven/org.hamcrest/hamcrest-core/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/validation-api-1.1.0.Final.jar][META-INF/maven/javax.validation/validation-api/pom.xml]->" + repoDir + "org/hamcrest/hamcrest-core/1.1/hamcrest-core-1.1.jar[META-INF/maven/org.hamcrest/hamcrest-core/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/validation-api-1.1.0.Final.jar][META-INF/maven/javax.validation/validation-api/pom.xml]->" + repoDir + "org/testng/testng/6.8/testng-6.8.jar[META-INF/maven/org.testng/testng/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/validation-api-1.1.0.Final.jar][META-INF/maven/javax.validation/validation-api/pom.xml]->" + repoDir + "org/testng/testng/6.8/testng-6.8.jar[META-INF/maven/org.testng/testng/pom.xml]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/validation-api-1.1.0.Final.jar][META-INF/maven/javax.validation/validation-api/pom.xml]->" + repoDir + "org/yaml/snakeyaml/1.6/snakeyaml-1.6.jar[META-INF/maven/org.yaml/snakeyaml/pom.properties]",
			"target/resources/test-war-0.1.war[WEB-INF/lib/validation-api-1.1.0.Final.jar][META-INF/maven/javax.validation/validation-api/pom.xml]->" + repoDir + "org/yaml/snakeyaml/1.6/snakeyaml-1.6.jar[META-INF/maven/org.yaml/snakeyaml/pom.xml]"
		);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
			actualList(actual, expected),
			ListUtils.isEqualList(expected, actual));
	}

	/*
	 * Should be "HOME/.m2/repository/" unless .crgrep has an alternative maven.localRepo setting
	 */
	private String getRepoBaseDir(Switches switches) {
		String localRepo = switches.getUserProperties() == null ? null : switches.getUserProperties().getProperty("maven.localRepo");
		if (localRepo != null) {
			return localRepo.endsWith("/") ? localRepo : localRepo + "/";
		}
		return getUserHome() + "/.m2/repository/";
	}
}
