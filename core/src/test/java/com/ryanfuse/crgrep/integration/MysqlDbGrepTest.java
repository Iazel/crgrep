/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.integration;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.junit.Test;

import com.ryanfuse.crgrep.ResourceGrep;
import com.ryanfuse.crgrep.TestCommon;
import com.ryanfuse.crgrep.util.DisplayRecorder;

/**
 * Integration test for all MySQL database tests
 * 
 * @author Craig Ryan
 */
public class MysqlDbGrepTest extends TestCommon {

	@Test
	public void testDbContentMysql() {
		title("mysql", "created", "*");
		List<String> expected = Arrays.asList(
				"CUSTOMER: [ID,CREATED]"
		);
		String[] args = new String[] { 
				"-d", 
				"-v",
//				"-X", "trace,debug",
				"-U", 
				"jdbc:mysql://localhost:3306/mydb",
				"--user=mydb", "--password=password", 
				"CREATED", "*"
		};
		ResourceGrep rg = new ResourceGrep(args);
		display = new DisplayRecorder(rg.getDisplay());
		rg.setDisplay(display);
		rg.execute();
		List<String> actual = display.messages();
		assertTrue(
			actualList(actual, expected),
			ListUtils.isEqualList(expected, actual));
	}

}
