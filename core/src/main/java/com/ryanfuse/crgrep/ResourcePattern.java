/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.regex.Pattern;


/**
 * An interface for resource patterns.
 * 
 * Supports the conversion between RE and Database patterns.
 * 
 * @author Craig Ryan
 */
public class ResourcePattern {

	private String pattern; // the command line pattern (wildcard string)
	private String rePattern; 
	private String dbPattern;

	public ResourcePattern(String pat) {
		this.pattern = pat;
		// convert to DB wildcarded string
		dbPattern = pat.replace('*', '%').replace('?', '_');
		// convert from wildcard to valid RE
		rePattern = pat.replace("?", ".?").replace("*", ".*?");
	}

	public String getPatternString() {
		return pattern;
	}
	
	public Pattern getPattern() {
		return Pattern.compile(rePattern);
	}

	public String getRegularExpression() {
		return rePattern;
	}
	
	public String getDatabaseExpression() {
		return dbPattern;
	}

	public String getDatabaseWildcardExpression() {
		String wcPat = dbPattern;
		if (!dbPattern.startsWith("%")) {
			wcPat = "%" + dbPattern;
		}
		if (!dbPattern.endsWith("%")) {
			wcPat = wcPat + "%";
		}
		return wcPat;
	}

	public boolean isDatabaseExpressionWildcard() {
		return dbPattern.equals("%");
	}
	public boolean isRegularExpressionWildcard() {
		return rePattern.equals(".*");
	}
	public boolean databaseExpressionContainsWildcard() {
		return dbPattern.contains("%") || dbPattern.contains("_");
	}
	public boolean regularExpressionContainsWildcard() {
		return rePattern.contains(".?") || dbPattern.contains(".*");
	}
}
