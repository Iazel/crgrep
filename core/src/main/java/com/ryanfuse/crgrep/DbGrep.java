/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;

import org.apache.commons.lang.StringUtils;

import com.ryanfuse.crgrep.db.DatabaseFactory;
import com.ryanfuse.crgrep.db.DatabaseParser;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Grep a database for matches against column names and optionally column data (content).
 * 
 * @author Craig Ryan
 */
public class DbGrep extends AbstractGrep {

	private DatabaseParser par;
	
	public DbGrep(Switches switches) {
		super(switches);
	}

	@Override
	public void execute() {
		par = DatabaseFactory.getDatabaseParser(getUri(), getUser(), getPw());
		if (par == null) {
			getDisplay().error("crgrep: unknown/unsupported database with URI " + getUri());
			return;
		}
		par.setDisplay(getDisplay());
		par.setResourceList(getResourceList());
		try {
			grepDatabase(par);
			par.cleanup();
		} catch (Exception e) {
			e.printStackTrace();
			getDisplay().error("crgrep: failed database grep [" + e.getLocalizedMessage() + "]");
		}
	}
	
	/*
	 * Generic entry point for all DB parser implementations. 
	 */
	private void grepDatabase(DatabaseParser par) throws Exception {
		// Example command line for listing only (no content) match will be:
		//	 	crgrep  "*"  FOO.*       ie the resource list chooses the tables/columns of interest and pattern '*' matches all of them.
		// Less common:
		//      crgrep  BAH  "FOO.*"     only searches table FOO and then matches the table/column name(s) on pattern 'BAH' 
		//
		Map<String, List<String>> tableMap = par.getFullTableMap();
		if (tableMap == null) {
			getDisplay().warn("Database has no tables!");
			return;
		}
		Set<Entry<String, List<String>>> entrySet = tableMap.entrySet();
		for (Entry<String, List<String>> e : entrySet) {
			String tab = e.getKey();
			debug("DBGrep MATCH table " + tab);
			String cols = StringUtils.join(e.getValue().toArray(), ",");
			// Of the filtered resources, attempt a match against <pattern>
			//System.out.println("DB TABLE FOUND: " + tab + "." + cols);
			StringBuilder cb = new StringBuilder();
			cb.append(tab).append(": ").append(cols);
			Matcher pm = getResourcePattern().getPattern().matcher(cb);
			if (pm.find()) {
				getDisplay().result(tab + ": [" + cols + "]");
			}

			// content grep - grep all char data in each table (content) or all data (allContent)
			if (isContent() || isAllcontent()) {
				debug("DbGrep Table Contents> " + tab);
	
				List<String> columns = null;
				if (isAllcontent()) {
					columns = tableMap.get(tab); // all columns
				} else {
					// Only columns we're interested in.
					columns = par.getColumnSubset(tab);
				}
				List<List<String>> rs = par.getMatchingColumns(columns, tab, getResourcePattern());
				if (rs != null) {
					// Matched on subset of columns, but display ALL column data in the result
					displayTableHits(tab, tableMap.get(tab), rs);
				}
			}
		}
	}

	/*
	 * Display all rows from the table which match the grep criteria. Results are given as CSV.
	 */
	private void displayTableHits(String table, List<String> cols, List<List<String>> rs) throws Exception {
		StringBuilder sb = new StringBuilder();
		int colCount = cols.size();
		Iterator<List<String>> iter = rs.iterator();
		while (iter.hasNext()) {
			List<String> row = iter.next();
			sb.setLength(0);
			sb.append(table).append(": ");
			for (int i = 0; i < colCount; i++) {
				String val = row.get(i);
				if (i > 0) {
					sb.append(",");
				}
				sb.append(val == null ? "" : val);
			}
			getDisplay().result(sb.toString());
		}
	}
}
