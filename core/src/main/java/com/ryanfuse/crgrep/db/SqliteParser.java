/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ryanfuse.crgrep.ResourcePattern;

/**
 * Parser class to query and parse data from an Sqlite database.
 * 
 * @author Craig Ryan
 */
public class SqliteParser extends DatabaseParser {
	
	private static final String SQLITE_DEF_USER = "root";
	private static final String SQLITE_DEF_PW = "password";
	private static final String LOG_PREFIX = "Sqlite";
	private static final String ALL_TABLES = "SQLITE_MASTER";
	private static final String COLUMN_TYPE_REGEX = ".*CHAR.*|TEXT|.*DATETIME";
	
	private Connection conn = null;
	private Driver dbDriver;
	private String schemaName;
	Map<String, List<String[]>> tableColumnTypeMap = new HashMap<String, List<String[]>>();
			
	public SqliteParser(String uri, String user, String password) {
		super(uri, user, password);
	}

	@Override
	public String getDefaultUser() {
		return SQLITE_DEF_USER;
	}

	@Override
	public String getDefaultPassword() {
		return SQLITE_DEF_PW;
	}

	public String getLogPrefix() {
		return LOG_PREFIX;
	}
	
	@Override
	public Connection dbConnection() throws Exception {
		if (conn != null && !conn.isClosed()) {
			return conn;
		}
		String uri = getUri();
		if (uri.equals("sqlite://") || uri.equals("sqlite://:memory")) {
			throw new RuntimeException("SQLite in-memory database cannot be searched.");
		}
		dbDriver = (Driver) Class.forName ("org.sqlite.JDBC").newInstance();
		conn = DriverManager.getConnection(getUri(), getUser(), getPassword());
		return conn;
	}

	public void dbClose() throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	/*
	 * A map with key = table name and values a list of columns for that table.
	 * 
	 * @see com.ryanfuse.crgrep.db.DatabaseParser#getFullTableMap()
	 */
	@Override
	public Map<String, List<String>> getFullTableMap() throws Exception {
		StringBuilder allColsQuery = new StringBuilder("select tbl_name, sql from ");
		allColsQuery.append(ALL_TABLES).append(" WHERE type='table' and tbl_name not like 'sqlite_%'");
		if (schemaName != null) {
			allColsQuery.append(" AND TABLE_SCHEMA = '").append(schemaName).append("'");
		}

		/*
		 * Table/column syntax:
		 *    table.col  	exact column
		 *    .col 			all tables with specific column
		 *    table 		all columns from table
		 *    table*.*col? - limited wildcarded table name(s) and column(s)
		 */
		String tabFilter = dbResourceListFilter();
		if (tabFilter != null) {
			allColsQuery.append(" AND ").append(tabFilter);
		}
		trace("DB TABLE COLUMNS QUERY: " + allColsQuery.toString());
		ResultSet rs = queryDb(dbConnection(), allColsQuery.toString());
		Map<String, List<String>> tableMap = mapTableColumns(rs);
		return tableMap;
	}

	/*
	 * Subset of a tables columns which match required data types. Allows the grep 
	 * to search on text or date data etc and ignore binary values.
	 * 
	 * @see com.ryanfuse.crgrep.db.DatabaseParser#getColumnSubset(java.lang.String)
	 */
	@Override
	public List<String> getColumnSubset(String table) throws Exception {
		List<String[]> tableCols = tableColumnTypeMap.get(table);
		List<String> colSubset = new ArrayList<String>();
		trace("Try match columns to [" + COLUMN_TYPE_REGEX + "]");
		for (int i = 0; i < tableCols.size(); i++) {
			String[] cols = tableCols.get(i);
			String colName = cols[0];
			String type = cols[1];
			if (type.matches(COLUMN_TYPE_REGEX)) {
				colSubset.add(colName);
				trace("Column " + colName + " matches!");
			} else {
				trace("Column " + colName + " NO match");
			}
		}
		return colSubset;
	}

	/*
	 * All rows whose data matches the regular expression, returned as a list of row data.
	 * 
	 * @see com.ryanfuse.crgrep.db.DatabaseParser#getMatchingColumns(java.util.List, java.lang.String, com.ryanfuse.crgrep.ResourcePattern)
	 */
	@Override
	public List<List<String>> getMatchingColumns(List<String> columnList, String table, ResourcePattern pattern) throws Exception {
		String dbPattern = pattern.getDatabaseWildcardExpression();
		String colCondition = buildColumnQuery(dbPattern, columnList);
		if (colCondition == null) {
			// no columns of the required type
			return null;
		}
		StringBuilder dataQuery = new StringBuilder();
		dataQuery.append("SELECT * from ").append(table);
		dataQuery.append(colCondition);
		trace("DB CONTENT QUERY> " + dataQuery.toString());
		ResultSet rs = queryDb(dbConnection(), dataQuery.toString());
		return resultSetToList(rs);
	}
	
	/*
	 * Build a map keyed by table name, and all columns in the result set (value=list of column names)
	 */
	private Map<String, List<String>> mapTableColumns(ResultSet rs) throws SQLException {
		Map<String, List<String>> tabs = new HashMap<String, List<String>>();
		while (rs.next()) {
			String tab = rs.getString("TBL_NAME");
			String sql = rs.getString("SQL");
			if (!tabs.containsKey(tab)) {
				tabs.put(tab, new ArrayList<String>());
			}
			List<String> tabCols = extractTableColumns(tab, sql);
			for (String col : tabCols) {
				trace("Map table [" + tab + "]   column [" + col + "]");
				tabs.get(tab).add(col);
			}
		}
		rs.close();
		return tabs;
	}

	/*
	 * Extract the columns list based on a query of SQLITE_MASTER which contains a 'CREATE TABLE..'
	 * sql string containing column information. This method extracts the column info from the
	 * query result string.
	 */
	private List<String> extractTableColumns(String table, String sql) {
		String col_list = sql.replaceAll("[^(]*\\((.*)\\)", "$1") // strip CREATE TABLE.. etc noise
				.replaceAll("\\([^(]*\\)", "") // strip all len '(xxx)'
				.replaceAll("\"([^ ]+)\" ", "$1=") // id=type xxx,...
				.replaceAll("(\\w+=\\w+) [^,]*", "$1"); // id=type xxx,...
		String[] cols = col_list.split(",");
		List<String> tableCols = new ArrayList<String>();
		List<String[]> tableColumnTypePairs = new ArrayList<String[]>();
		for (String colpair : cols) {
			String[] cp = colpair.split("=");
			String c = cp[0].trim();
			String t = cp[1].trim().toUpperCase();
			tableCols.add(c);
			tableColumnTypePairs.add(new String[]{c, t});
		}
		// Also store all column/type pairs for the table for later reference.
		if (!tableColumnTypePairs.isEmpty()) {
			tableColumnTypeMap.put(table, tableColumnTypePairs);
		}
		return tableCols;
	}

	/*
	 * Filter of tables and or columns based on limited RE.
	 *  table.col  		exact column
	 *  .col 			all tables with specific column
	 *  table 			all columns from table
	 *  table*.*col? 	limited wildcarded table name(s) and column(s)
	 */
	private String dbResourceListFilter() {
		String tab = null;
		debug("Resource list: " + getResourceList().getSingleResourcePath());
		if (!getResourceList().isWild()) {
			if (!getResourceList().isFirstPartWild()) {
				tab = getResourceList().firstPart();
			}
		}
		StringBuilder q = new StringBuilder();
		if (tab != null) {
			if (getResourceList().firstPartContainsWild()) {
				q.append("TBL_NAME LIKE '").append(tab).append("'");
			} else {
				q.append("TBL_NAME = '").append(tab).append("'");
			}
		}
		trace("TABLE LIST FILTER: " + q.toString());
		return q.length() == 0 ? null : q.toString();
	}
}
