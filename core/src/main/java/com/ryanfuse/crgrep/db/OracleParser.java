/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ryanfuse.crgrep.ResourcePattern;

/**
 * Parser class to query and parse data from an Oracle database.
 * 
 * @author Craig Ryan
 */
public class OracleParser extends DatabaseParser {
	
	private static final String ORA_DEF_USER = "sysdba";
	private static final String ORA_DEF_PW = "password";
	private static final String LOG_PREFIX = "Oracle";
	
	private Connection conn = null;
	private String tables = null;

	public OracleParser(String uri, String user, String password) {
		super(uri, user, password);
		tables = getUser().equals(ORA_DEF_USER) ? "all_tab_columns" : "user_tab_columns";
	}

	@Override
	public String getDefaultUser() {
		return ORA_DEF_USER;
	}

	@Override
	public String getDefaultPassword() {
		return ORA_DEF_PW;
	}

	public String getLogPrefix() {
		return LOG_PREFIX;
	}

	@Override
	public Connection dbConnection() throws Exception {
		if (conn != null && !conn.isClosed()) {
			return conn;
		}
		Class.forName ("oracle.jdbc.driver.OracleDriver");
		conn = DriverManager.getConnection(getUri(), getUser(), getPassword());
		debug("DB CONNECT: user=" + getUser() + ",pw=" + getPassword() + ",uri=" + getUri().substring(0,40) + "...");
		return conn;
	}

	public void dbClose() throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}
	
	@Override
	public Map<String, List<String>> getFullTableMap() throws Exception {
		String allColsQuery = "select TABLE_NAME, COLUMN_NAME from " + tables;
		/*
		 * Table/column syntax:
		 *    table.col  	exact column
		 *    .col 			all tables with specific column
		 *    table 		all columns from table
		 *    table*.*col? - limited wildcarded table name(s) and column(s)
		 */
		String tabFilter = dbResourceListFilter();
		
		String query = allColsQuery + (tabFilter == null ? "" : " WHERE " + tabFilter);
		trace("DB TABLE COLUMNS QUERY: " + query);		
		ResultSet rs = queryDb(dbConnection(), query);
		Map<String, List<String>> tableMap = mapTableColumns(rs);
		return tableMap;
	}

	@Override
	public List<String> getColumnSubset(String table) throws Exception {
		String colsQuery = "select COLUMN_NAME from " + tables + " where data_type in ('VARCHAR2','VARCHAR','CHAR', 'NCHAR', 'NVARCHAR2','XML','URI','DATE')";
		debug("DB CONTENT SUBSET QUERY> " + colsQuery + " AND TABLE_NAME = '" + table + "'");
		ResultSet rs = queryDb(dbConnection(), colsQuery + " AND TABLE_NAME = '" + table + "'"); // non numeric/blob columns
		return listColumns(rs);
	}

	@Override
	public List<List<String>> getMatchingColumns(List<String> columnList, String table, ResourcePattern pattern) throws Exception {
		String dbPattern = pattern.getDatabaseWildcardExpression();
		String colCondition = buildColumnQuery(dbPattern, columnList);
		if (colCondition == null) {
			// no columns of the required type
			return null;
		}
		StringBuilder dataQuery = new StringBuilder();
		dataQuery.append("SELECT * from ").append(table);
		dataQuery.append(colCondition);
		debug("DB CONTENT QUERY> " + dataQuery.toString());
		ResultSet rs = queryDb(dbConnection(), dataQuery.toString());
		return resultSetToList(rs);
	}
	
	/*
	 * Build a map keyed by table name, and all columns in the result set (value=list of column names)
	 */
	private Map<String, List<String>> mapTableColumns(ResultSet rs) throws SQLException {
		Map<String, List<String>> tabs = new HashMap<String, List<String>>();
		while (rs.next()) {
			String tab = rs.getString("TABLE_NAME");
			String col = rs.getString("COLUMN_NAME");
			if (!tabs.containsKey(tab)) {
				debug("Map table [" + tab + "]   column [" + col + "]");
				tabs.put(tab, new ArrayList<String>());
			}
			tabs.get(tab).add(col);
		}
		rs.close();
		return tabs;
	}

	/*
	 * Build a list of all columns in the result set
	 */
	private List<String> listColumns(ResultSet rs) throws SQLException {
		List<String> cols = new ArrayList<String>();
		while (rs.next()) {
			String col = rs.getString("COLUMN_NAME");
			if (!cols.contains(col)) {
				cols.add(col);
			}
		}
		rs.close();
		return cols;
	}
	
	/*
	 * Filter of tables and or columns based on limited RE.
	 *  table.col  		exact column
	 *  .col 			all tables with specific column
	 *  table 			all columns from table
	 *  table*.*col? 	limited wildcarded table name(s) and column(s)
	 */
	private String dbResourceListFilter() {
		String tab = null;
		String col = null;
		debug("Resource list: " + getResourceList().getSingleResourcePath());
		if (!getResourceList().isWild()) {
			if (!getResourceList().isFirstPartWild()) {
				tab = getResourceList().firstPart();
			}
			if (!getResourceList().isLastPartWild()) { 
				col = getResourceList().lastPart();
			}
		}
		StringBuilder q = new StringBuilder();
		if (tab != null) {
			if (getResourceList().firstPartContainsWild()) {
				q.append("TABLE_NAME LIKE '").append(tab).append("'");
			} else {
				q.append("TABLE_NAME = '").append(tab).append("'");
			}
		}
		if (col != null) {
			if (q.length() > 0) {
				q.append(" AND ");
			}
			if (getResourceList().lastPartContainsWild()) {
				q.append("COLUMN_NAME LIKE '").append(col).append("'");
			} else {
				q.append("COLUMN_NAME = '").append(col).append("'");
			}
		}
		debug("TABLE LIST FILTER: " + q.toString());
		return q.length() == 0 ? null : q.toString();
	}
}
