/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ryanfuse.crgrep.ResourceList;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.util.Display;

/**
 * Base class to be extended by all Database table parsers.
 *
 * @author Craig Ryan
 */
public abstract class DatabaseParser {

	protected String uri;
	protected String user;
	protected String password;
	private boolean verbose;
	private Display display;
	
	private ResourceList<String> resourceList;
	private Statement stmt;
	
	public DatabaseParser(String uri, String user, String password) {
		this.uri = uri;
		if (user == null) {
			user = getDefaultUser();
		}
		if (password == null) {
			password = getDefaultPassword();
		}
		this.user = user;
		this.password = password;
	}

	public ResultSet queryDb(Connection conn, String query) throws Exception {
		if (stmt != null) {
			stmt.close();
		}
		stmt = conn.createStatement();
		stmt.setQueryTimeout(30);
		return stmt.executeQuery(query);
	}
	
	/**
	 * The JDBC URI for the DB
	 * 
	 * @return
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * DB user specified on command line
	 * @return
	 */
	public String getUser() {
		return user;
	}

	/**
	 * The DB password specified on the command line 
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Default user typically used for this DB vendor if none is specified.
	 * 
	 * @return
	 */
	public abstract String getDefaultUser();
	
	/**
	 * Default password typically used for this DB vendor if none is specified.
	 * @return
	 */
	public abstract String getDefaultPassword();
	
	/**
	 * Logger prefix to identify the parser implementation class 
	 * 
	 * @return
	 */
	public abstract String getLogPrefix();
	
	/**
	 * Open a connection to this vendors DB
	 * 
	 * @return a connection to the db
	 * @throws Exception unable to connect to the database (server not running?)
	 */
	public abstract Connection dbConnection() throws Exception;

	/**
	 * Close the db
	 * 
	 * @throws Exception
	 */
	public abstract void dbClose() throws Exception;
	
	/**
	 * Return a map of all tables in the specified database. For each, map a list of columns.
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, List<String>> getFullTableMap() throws Exception;
	
	/**
	 * Generate a map of all columns for the given table which are grep'able (non-binary columns).
	 * 
	 * @param table the table name
	 * @return list of table column names which are grep'able
	 * @throws Exception db error
	 */
	public abstract List<String> getColumnSubset(String table) throws Exception;
	
	/**
	 * Returns the columns matching the pattern
	 *  
	 * @param columnList
	 * @param table
	 * @param pattern
	 * @return
	 * @throws Exception
	 */
	public abstract List<List<String>> getMatchingColumns(List<String> columnList, String table, ResourcePattern pattern) throws Exception;
	
	/*
	 * Convert ResultSet of column data to a List where each entry is a row of data values. 
	 */
	protected List<List<String>> resultSetToList(ResultSet rs) {
		List<List<String>> l = new ArrayList<List<String>>();
		try {
			int size = rs.getMetaData().getColumnCount();
			while (rs.next()) {
				List<String> ls = new ArrayList<String>();
				for (int i = 1; i <= size; i++) {
					ls.add(rs.getString(i));
				}
				l.add(ls);
			}
			rs.close();
		} catch (Exception e) {
			getDisplay().error("Failed to read ResultSet data, error: " + e.getLocalizedMessage());
		}
		return l;
	}

	/*
	 * Build a SQL query for columns which match the DB pattern.
	 * Basic format "select .. from <t> where c1 like '%pat%' or c2 like..."
	 * This method generates the 'where..' part of the query.
	 */
	protected String buildColumnQuery(String dbPattern, List<String> cols) {
		if (cols == null) {
			return null;
		}
		boolean first = true;
		boolean re = dbPattern.contains("%") || dbPattern.contains("_");
		StringBuilder q = new StringBuilder();
		for (String col : cols) {
			if (first) {
				q.append(" WHERE ");
			} else {
				q.append(" OR ");
			}
			q.append(col).append(re ? " LIKE '" : " = '").append(dbPattern).append("'");
			first = false;
		}
		return q.toString();
	}

	public void cleanup() throws Exception {
		dbClose(); 
	}

	public ResourceList<String> getResourceList() {
		return resourceList;
	}

	public void setResourceList(ResourceList<String> resourceList) {
		this.resourceList = resourceList;
	}
	
	public void trace(String msg) {
		getDisplay().trace(msg);
	}

	public void debug(String msg) {
		getDisplay().debug(msg);
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public Display getDisplay() {
		return display;
	}

	public void setDisplay(Display display) {
		this.display = display;
		getDisplay().setLogPrefix(getLogPrefix());
	}
}
