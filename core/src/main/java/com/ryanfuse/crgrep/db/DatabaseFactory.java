/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.db;

/**
 * Get a database parser determined by the DB URI.
 * 
 * @author Craig Ryan
 */
public class DatabaseFactory {

	/*
	 * Return a parser or null if DB type is not supported.
	 */
	public static DatabaseParser getDatabaseParser(String uri, String user, String pw) {
		DatabaseParser par = null;
		if (uri.startsWith("jdbc:mysql")) {
			par = new MysqlParser(uri, user, pw);
		} else if (uri.startsWith("jdbc:ora")) {
			par = new OracleParser(uri, user, pw);
		} else if (uri.startsWith("jdbc:sqlite")) {
			par = new SqliteParser(uri, user, pw);
		} else if (uri.startsWith("jdbc:h2")) {
			par = new H2Parser(uri, user, pw);
		}
		return par;
	}
}
