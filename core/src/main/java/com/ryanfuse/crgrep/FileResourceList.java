/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.List;

/**
 * List of all file resources to be grep'ed, represented as a wildcard pattern.
 * 
 * @author Craig Ryan
 */
public class FileResourceList implements ResourceList<String> {

	List<String> listPattern;
	int idx = 0;
	boolean wild = false;
	String first, last, path;

	public int size() {
		return listPattern.size();
	}

	@Override
	public String get(int index) {
		return listPattern.get(index);
	}

	@Override
	public boolean hasNext() {
		return idx < listPattern.size();
	}

	@Override
	public String next() {
		setIndicators();
		return listPattern.get(idx++);
	}

	@Override
	public void remove() {
		// no-op
	}

	public FileResourceList(List<String> fileList) {
		this.listPattern = fileList;
	}
	
	private void setIndicators() {
		String currentPath = listPattern.get(idx); 
		if (currentPath.equals("*") || currentPath.equals(".") || currentPath.equals("*.*") || currentPath.equals("/")) {
			wild = true;
		}
		String name = null;
		String ext = null;
		String rest = null;
		if (currentPath.contains("/")) {
			int slash = currentPath.lastIndexOf("/");
			path = slash == 0 ? "/" : currentPath.substring(0, slash);
			rest = slash+1 >= currentPath.length() ? null : currentPath.substring(slash+1);
		} else {
			path = null;
			rest = currentPath;
		}
		if (rest != null) {
			if (rest.contains(".")) {
				int dot = rest.indexOf(".");
				name = dot == 0 ? null : rest.substring(0, dot);
				ext = dot+1 >= rest.length() ? null : rest.substring(dot+1);
			} else {
				name = currentPath; // no extent
			}
			ext = ext == null ? null : (ext.equals("*") ? null : ext);
			name = name == null ? null : (name.equals("*") ? null : name);
		}
		first = name;
		last = ext;
	}
	
	public String firstPart() {
		return first;
	}

	public String lastPart() {
		return last;
	}

	public boolean isWild() {
		return wild;
	}
	
	public boolean isFirstPartWild() {
		return wild ? true : first == null ? true : first.equals("*");
	}

	public boolean isLastPartWild() {
		return wild ? true : last == null ? true : last.equals("*");
	}

	public String getSingleResourcePath() {
		return listPattern.get(0);
	}

	public String pathPart() {
		return path;
	}

	public boolean isPathWild() {
		return wild ? true : path == null ? true : path.equals("*");
	}

	public boolean pathContainsWild() {
		return wild ? true : path == null ? true : path.contains("*") || path.contains("?") ;
	}

	public boolean firstPartContainsWild() {
		return wild ? true : first == null ? true : first.contains("*") || first.contains("?") ;
	}

	public boolean lastPartContainsWild() {
		return wild ? true : last == null ? true : last.contains("*") || last.contains("?") ;
	}
}
