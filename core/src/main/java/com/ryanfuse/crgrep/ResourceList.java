/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.util.Iterator;

/**
 * Manages Resource list patterns provided on the command line.
 * 
 * Each implementation interprets the meaning of the list syntax, in general though there
 * are three parts: the path, the resource name and resource extent. ie 'path<seperator>name.extent'. 
 * 
 * @author Craig Ryan
 * @param <E>
 */
public interface ResourceList<E> extends Iterator<E> {
	// number of items in the list
	int size();
	// List op to access items by index
	E get(int index);
	// the entire pattern, assumes there is only one
	String getSingleResourcePath();
	// the path
	String pathPart();
	// the resource name
	String firstPart();
	// the resource extent
	String lastPart();
	// is the entire path wildcarded
	boolean isWild();
	// is the path wild
	boolean isPathWild();
	// does the path contain any wildcard chars
	boolean pathContainsWild();
	// is the name wild
	boolean isFirstPartWild();
	// does the name contain any wildcard chars?
	boolean firstPartContainsWild();
	// is the extent wild
	boolean isLastPartWild();
	// does the extent contain any wildcard chars?
	boolean lastPartContainsWild();
}
