/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

/**
 *  List of all database resources to be grep'ed, represented by a wildcard expression.
 * 
 * @author Craig Ryan
 */
public class DbResourceList implements ResourceList<String> {

	private String listPattern;
	private boolean wild = false;
	private boolean patternAccessed = false;
	private String first, last;
	private ResourcePattern dbFirst;
	private ResourcePattern dbLast;
	
	public int size() {
		return 1;
	}
	
	@Override
	public String get(int index) {
		if (index > 0) {
			throw new IndexOutOfBoundsException("Database resource list does not support more than one item, attempted to index item [" + index + "]");
		}
		return listPattern;
	}
	
	@Override
	public boolean hasNext() {
		if (patternAccessed) {
			return false;
		}
		patternAccessed = true;
		return true;
	}

	@Override
	public String next() {
		return listPattern;
	}

	@Override
	public void remove() {
		// no-op
	}

	public DbResourceList(String pat) {
		this.listPattern = pat;
		if (pat.equals("*") || pat.equals("*.*") || pat.equals(".*") || pat.equals("*.")) {
			wild = true;
		}
		String tab = null;
		String col = null;
		if (pat.contains(".")) {
			int dot = pat.indexOf(".");
			tab = dot == 0 ? "*" : pat.substring(0, dot);
			col = dot+1 >= pat.length() ? "*" : pat.substring(dot+1);
		} else {
			// just a table name
			tab = pat;
			col = "*";
		}
		first = tab;
		last = col;
		dbFirst = new ResourcePattern(first);
		dbLast = new ResourcePattern(last);
	}
	
	public String firstPart() {
		return dbFirst.getDatabaseExpression();
	}

	public String lastPart() {
		return dbLast.getDatabaseExpression();
	}

	public boolean isWild() {
		return wild;
	}
	
	/*
	 * if *.XXX
	 * @see com.ryanfuse.crgrep.ResourceList#isFirstPartWild()
	 */
	public boolean isFirstPartWild() {
		return wild ? true : dbFirst.isDatabaseExpressionWildcard();
	}

	/*
	 * if XXX.*
	 * @see com.ryanfuse.crgrep.ResourceList#isFirstPartWild()
	 */
	public boolean isLastPartWild() {
		return wild ? true : dbLast.isDatabaseExpressionWildcard();
	}

	/*
	 * if first part contains any wild chars.
	 * @see com.ryanfuse.crgrep.ResourceList#isFirstPartWild()
	 */
	public boolean firstPartContainsWild() {
		return wild ? true : dbFirst.databaseExpressionContainsWildcard();
	}

	/*
	 * if second part contains any wild chars.
	 * @see com.ryanfuse.crgrep.ResourceList#isFirstPartWild()
	 */
	public boolean lastPartContainsWild() {
		return wild ? true : dbLast.databaseExpressionContainsWildcard();
	}

	public String getSingleResourcePath() {
		return listPattern;
	}

	public String pathPart() {
		return null;
	}

	public boolean isPathWild() {
		return true; // db resource list has no path
	}

	public boolean pathContainsWild() {
		return true;
	}

}
