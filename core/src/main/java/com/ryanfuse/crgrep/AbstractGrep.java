/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Abstract base class for all resource grep implementations.
 * 
 * Holds the complete set of command line options specified.
 * 
 * @author Craig Ryan
 */
public abstract class AbstractGrep {
	private String user;
	private String pw;
	private String uri;
	private ResourcePattern resourcePattern;
	private ResourceList<String> resourceList;
	private Display display;
    private Switches switches;

	public AbstractGrep(Switches switches) {
        this.switches = switches;
        display = new Display();
		display.setDebugOn(switches.isDebug());
		display.setTraceOn(switches.isTrace());
		display.setWarnOn(switches.isVerbose());
	}
	
	public abstract void execute();

	public void setArguments(ResourcePattern pattern, ResourceList<String> resourceList) {
		this.resourcePattern = pattern;
		this.resourceList = resourceList;
	}
	
	public void trace(String msg) {
		getDisplay().trace(msg);
	}

	public void debug(String msg) {
		getDisplay().debug(msg);
	}

	public boolean isVerbose() {
		return switches.isVerbose();
	}

	public boolean isRecurse() {
		return switches.isRecurse();
	}

	public boolean isContent() {
		return switches.isContent();
	}

	public boolean isAllcontent() {
		return switches.isAllcontent();
	}

    public boolean isListing() {
        return switches.isListing();
    }

    public boolean isMaven() {
        return switches.isMaven();
    }

	public Switches getSwitches() {
        return switches;
    }

    public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public ResourcePattern getResourcePattern() {
		return resourcePattern;
	}

	public void setResourcePattern(ResourcePattern resourcePattern) {
		this.resourcePattern = resourcePattern;
	}

	public ResourceList<String> getResourceList() {
		return resourceList;
	}

	public void setResourceList(ResourceList<String> resourceList) {
		this.resourceList = resourceList;
	}

	public Display getDisplay() {
		return display;
	}

	public void setDisplay(Display display) {
		this.display = display;
	}

	protected void ignoreResource(String fname, String err) {
		getDisplay().error("Error reading '" + fname + "' (skipped), cause: " + err);
	}
}
