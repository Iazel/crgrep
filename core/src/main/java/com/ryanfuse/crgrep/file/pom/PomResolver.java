/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.pom;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.internal.DefaultArtifactDescriptorReader;
import org.apache.maven.repository.internal.DefaultVersionRangeResolver;
import org.apache.maven.repository.internal.DefaultVersionResolver;
import org.apache.maven.repository.internal.MavenRepositorySystemSession;
import org.sonatype.aether.RepositorySystem;
import org.sonatype.aether.RepositorySystemSession;
import org.sonatype.aether.artifact.Artifact;
import org.sonatype.aether.collection.CollectRequest;
import org.sonatype.aether.connector.async.AsyncRepositoryConnectorFactory;
import org.sonatype.aether.connector.file.FileRepositoryConnectorFactory;
import org.sonatype.aether.connector.wagon.PlexusWagonConfigurator;
import org.sonatype.aether.connector.wagon.WagonConfigurator;
import org.sonatype.aether.connector.wagon.WagonRepositoryConnectorFactory;
import org.sonatype.aether.graph.Dependency;
import org.sonatype.aether.graph.DependencyFilter;
import org.sonatype.aether.impl.ArtifactDescriptorReader;
import org.sonatype.aether.impl.VersionRangeResolver;
import org.sonatype.aether.impl.VersionResolver;
import org.sonatype.aether.impl.internal.DefaultRepositorySystem;
import org.sonatype.aether.impl.internal.DefaultServiceLocator;
import org.sonatype.aether.repository.LocalRepository;
import org.sonatype.aether.repository.RemoteRepository;
import org.sonatype.aether.resolution.ArtifactResult;
import org.sonatype.aether.resolution.DependencyRequest;
import org.sonatype.aether.resolution.DependencyResolutionException;
import org.sonatype.aether.resolution.DependencyResult;
import org.sonatype.aether.spi.connector.RepositoryConnectorFactory;
import org.sonatype.aether.transfer.AbstractTransferListener;
import org.sonatype.aether.transfer.TransferEvent;
import org.sonatype.aether.util.filter.DependencyFilterUtils;

import com.ryanfuse.crgrep.util.Display;

/**
 * Wraps the sonar aether api calls to resolve dependencies discovered in the POM, 
 * possible loading them into the local repository so we can process the resulting
 * jar (and other artifact) file types.
 * 
 * TODO: There may be an issue running behind a firewall, need to update this code to
 * properly make use of settings.xml proxy settings. In the meantime CRGREP may 
 * file with timeout errors (and display logging to that effect).
 * 
 * @author Craig Ryan
 */
public class PomResolver {
	
    private final RepositorySystem repoSystem = buildRepoSystem();
	private List<RemoteRepository> remoteRepos;
	private File localRepoFolder;
	private Display display;

	public PomResolver(MavenProject proj, File localRepoFolder, Display display) {
		this.localRepoFolder = localRepoFolder;
		this.display = display;
		this.remoteRepos = proj.getRemoteProjectRepositories();
	}

	public List<Artifact> resolve(Artifact rootArtifact, String scope) throws DependencyResolutionException {
		DependencyFilter filter = DependencyFilterUtils.classpathFilter(scope);
		if (filter == null) {
			throw new IllegalStateException(String.format("Failed to create a POM scope filter for '" + scope + "'"));
		}
		Dependency rootDep = new  Dependency(rootArtifact, scope);
		CollectRequest collectRequest = new CollectRequest();
        collectRequest.setRoot(rootDep);
        for (RemoteRepository remoteRepo : remoteRepos) {
            if (!remoteRepo.getProtocol().matches("http?|file|s3")) {
            	display.warn("Ignoring remote maven respository '" + remoteRepo.getProtocol() + "' (unknown protocol)");
                continue;
            }
            collectRequest.addRepository(remoteRepo);
        }
		List<Artifact> deps = new LinkedList<Artifact>();
        try {
            Collection<ArtifactResult> artifactResults;
            DependencyRequest depRequest = new DependencyRequest(collectRequest, filter);
            synchronized (this.localRepoFolder) {
            	DependencyResult depResult = repoSystem.resolveDependencies(repoSystemSession(), depRequest);
                artifactResults = depResult.getArtifactResults();
            }
            for (ArtifactResult res : artifactResults) {
                deps.add(res.getArtifact());
            }
        } catch (Exception ex) {
        }
        return deps;
	}

	class PomTransferListener extends AbstractTransferListener {
		public void transferFailed(final TransferEvent event) {
        	display.debug("Transfer of dependency failed: " + event.getException().getLocalizedMessage());
	    }
	    public void transferCorrupted(final TransferEvent event) {
        	display.debug("Transfer of dependency corrupted: " + event.getException().getLocalizedMessage());
	    }
	    public void transferSucceeded(final TransferEvent event) {
        	display.debug("Transfer of dependency success: " + event.getResource().getResourceName());
	    }
	}
	
	private RepositorySystemSession repoSystemSession() {
		LocalRepository localRepo = new LocalRepository(this.localRepoFolder);
		MavenRepositorySystemSession session = new MavenRepositorySystemSession();
		session.setLocalRepositoryManager(
			repoSystem.newLocalRepositoryManager(localRepo)
		);
		session.setTransferListener(new PomTransferListener());
		return session;
	}
	
	private RepositorySystem buildRepoSystem() {
		DefaultServiceLocator locator = new DefaultServiceLocator();
        locator.addService(
        	RepositoryConnectorFactory.class,
            FileRepositoryConnectorFactory.class
        );
        locator.addService(
            RepositoryConnectorFactory.class,
            AsyncRepositoryConnectorFactory.class
        );
        locator.addService(
            WagonConfigurator.class,
            PlexusWagonConfigurator.class
        );
        locator.addService(
            RepositoryConnectorFactory.class,
            WagonRepositoryConnectorFactory.class
        );
        locator.addService(
            RepositorySystem.class,
            DefaultRepositorySystem.class
        );
        locator.addService(
            VersionResolver.class,
            DefaultVersionResolver.class
        );
        locator.addService(
            VersionRangeResolver.class,
            DefaultVersionRangeResolver.class
        );
        locator.addService(
            ArtifactDescriptorReader.class,
            DefaultArtifactDescriptorReader.class
        );
        RepositorySystem system = locator.getService(RepositorySystem.class);
        if (system == null) {
            throw new IllegalStateException("Failed to build a RepositorySystem for resolving POM dependencies.");
        }
		return system;
	}
	

}
