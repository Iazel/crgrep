/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.File;

import com.ryanfuse.crgrep.FileGrep;

/**
 * File type for Directory entries. This will basically grep against the directory name
 * without navigating into sub-directories, the entire tree including all sub dirs has 
 * already been expanded by FileGrep.
 * 
 * @author Craig Ryan
 */
public class DirectoryFileType extends AbstractFileType {
    public DirectoryFileType(FileGrep fileGrep) {
        super(fileGrep);
    }

    @Override
    public boolean matchesSupportedType(File entry) {
    	getFileGrep().trace("DirectoryFileType '" + entry.getPath() + "' supported type? isDir=" + entry.isDirectory());
        return entry.isDirectory();
    }

    @Override
    public void grepEntry(File file) {
    	getFileGrep().debug("DirectoryFileType grep file '" + file.getPath() + "'");
        grepDirEntry(file);
    }
}
