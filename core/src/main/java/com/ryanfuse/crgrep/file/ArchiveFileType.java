
/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import com.ryanfuse.crgrep.FileGrep;

/**
 * File type for various supported types of archives.
 * 
 * @author Craig Ryan
 */
public class ArchiveFileType extends AbstractFileType {

    private static final String SUPPORTED_ARCHIVE_TYPES = ".*\\.(jar|ear|war|zip|tar|gz)";

    private Pattern autoExtentPattern = Pattern.compile("(jar|zip|tar)");
    
    public ArchiveFileType(FileGrep fileGrep) {
        super(fileGrep);
    }

    @Override
    public boolean matchesSupportedType(File entry) {
        boolean supported = isSupportedArchiveType(entry.getName());
    	getFileGrep().trace("ArchiveFileType '" + entry.getPath() + "' supported type? " + supported);
    	return supported;
    }

    @Override
    public boolean matchesSupportedFileName(String entryName) {
    	boolean supported = isSupportedArchiveType(entryName);
    	getFileGrep().trace("ArchiveFileType '" + entryName + "' supported stream type? " + supported);
    	return supported;
    }

    @Override
    public void grepEntry(File file) {
    	getFileGrep().debug("ArchiveFileType Grep file '" + file.getPath() + "'");
        BufferedInputStream bis = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            ArchiveInputStream input = createArchiveInputStream(file.getName(), bis);
            grepArchive(input, null, file.getPath());
        } catch (FileNotFoundException e) {
            ignoreResource(file.getPath(), e.getLocalizedMessage());
        } catch (ArchiveException e) {
            ignoreResource(file.getPath(), e.getLocalizedMessage());
        } catch (IOException e) {
            ignoreResource(file.getPath(), e.getLocalizedMessage());
		} finally {
            getFileGrep().closeResource(bis);
        }
    }

    @Override
    public void grepEntryStream(InputStream entryStream, String entryName, String entryPath) {
    	getFileGrep().debug("ArchiveFileType Grep stream " + streamEntryName(entryPath, entryName));
    	ArchiveInputStream nestedArchiveStream = null;
    	String fullName = streamEntryName(entryPath, entryName);
		try {
			nestedArchiveStream = createArchiveInputStream(entryName, entryStream);
		} catch (ArchiveException e) {
            ignoreResource(fullName, e.getLocalizedMessage());
            return;
        } catch (IOException e) {
            ignoreResource(fullName, e.getLocalizedMessage());
            return;
        }
		grepArchive(nestedArchiveStream, entryName, fullName);
    }
    
    private boolean isSupportedArchiveType(String fileName) {
        Pattern supportedArchiveExtent = Pattern.compile(SUPPORTED_ARCHIVE_TYPES);
        return supportedArchiveExtent.matcher(fileName).matches();
    }
    
    /*
     * Process a stream we know is a supported archive type
     */
    private ArchiveInputStream createArchiveInputStream(String fileName, InputStream bis) throws ArchiveException, IOException {
    	int extIdx = fileName.lastIndexOf('.');
    	if (extIdx == -1) {
    		// shouldn't happen since we already determined this to be a supported archive stream.
    		// Let the factory try guess the content type.
    		getFileGrep().debug("ArchiveFileType create archive i/p stream '" + fileName + "' no extent, call factory to guess stream type.");
    		return new ArchiveStreamFactory().createArchiveInputStream(bis);
    	}
    	String ext = fileName.substring(extIdx + 1 < fileName.length() ? extIdx + 1 : extIdx).toLowerCase();
    	ArchiveStreamFactory streamFactory = new ArchiveStreamFactory();
        if (autoExtentPattern.matcher(ext).matches()) {
    		getFileGrep().debug("ArchiveFileType create archive i/p stream '" + fileName + "' pass type '" + ext + "' factory to create stream.");
        	return streamFactory.createArchiveInputStream(ext, bis);
        }
        // special case - multiple extents .tar.gz
        if (fileName.endsWith(".tar.gz")) {
        	return streamFactory.createArchiveInputStream(ArchiveStreamFactory.TAR, new GzipCompressorInputStream(bis));
        }
        // factory doesn't automatically recognise extents like ear/war/7z, tell it to use ZIP 
		getFileGrep().debug("ArchiveFileType create archive i/p stream '" + fileName + "' pass ZIP to factory to create stream.");
        return streamFactory.createArchiveInputStream(ArchiveStreamFactory.ZIP, bis);
    }

    // Examine an archive and grep its contents.
    private void grepArchive(ArchiveInputStream input, String entryName, String entryPath) {
//        getFileGrep().debug("ArchiveFileType grep archive [" + archiveName + "] stream.");
        if (input instanceof ZipArchiveInputStream) {
            getFileGrep().debug("ArchiveFileType process ZIP stream " + entryPath);
            // ZIP, JAR, EAR, WAR
            ZipArchiveInputStream zis = (ZipArchiveInputStream)input;
            try {
                ZipArchiveEntry ze = null;
                while ((ze = zis.getNextZipEntry()) != null) {
                    grepArchiveEntryListing(entryPath, ze.getName());
                    if (!ze.isDirectory() && getFileGrep().isContent()) {
                        getFileGrep().debug("ArchiveFileType Grep archive entry content " + streamEntryName(entryPath , ze.getName()));
                        grepArchiveEntryContent(entryPath, ze, zis);
                    } else {
                        getFileGrep().debug("ArchiveFileType ZIP entry " + streamEntryName(entryPath , ze.getName()) + " ignored. (" + (getFileGrep().isContent() ? "isDir" : "noContent") + ")");
                    }
                }
            } catch (IOException e) {
                ignoreResource(entryPath, e.getLocalizedMessage());
            } catch (ArchiveException e) {
                ignoreResource(entryPath, e.getLocalizedMessage());
            }
        } else if (input instanceof TarArchiveInputStream) {
            getFileGrep().debug("ArchiveFileType process TAR stream path " + entryPath);
            // TAR
            TarArchiveInputStream tis = (TarArchiveInputStream)input;
            try {
                TarArchiveEntry te = null;
                while ((te = tis.getNextTarEntry()) != null) {
                    getFileGrep().debug("ArchiveFileType Grep archive entry content " + streamEntryName(entryPath , te.getName()));
                    grepArchiveEntryListing(entryPath, te.getName());
                    if (!te.isDirectory() && getFileGrep().isContent()) {
                        grepArchiveEntryContent(entryPath, te, tis);
                    } else {
                        getFileGrep().debug("ArchiveFileType TAR entry " + streamEntryName(entryPath , te.getName()) + " ignored. (" + (getFileGrep().isContent() ? "isDir" : "noContent") + ")");
                    }
                }
            } catch (IOException e) {
                ignoreResource(entryPath, e.getLocalizedMessage());
            } catch (ArchiveException e) {
                ignoreResource(entryPath, e.getLocalizedMessage());
            }
        }
    }

    /*
     * Grep the archive entry by name (listing).
     */
    private void grepArchiveEntryListing(String archivePath, String entryName) {
        //getFileGrep().trace("Grep ["+archiveName+"["+entryName+"]] against ["+ getResourcePattern().getPattern()+"]");
        Pattern pattern = getResourcePattern().getPattern();
        if (pattern.matcher(entryName).find()) {
            getDisplay().result(streamEntryName(archivePath, entryName));
        }
    }

    /*
     * Grep archive entries, including supported inner archive types (jars within wars etc). 
     */
    private void grepArchiveEntryContent(String entryPath, ArchiveEntry entry, ArchiveInputStream ais) throws ArchiveException {
        String entryName = entry.getName();
        getFileGrep().grepByContentsStream(ais, entryName, entryPath);
    }

}
