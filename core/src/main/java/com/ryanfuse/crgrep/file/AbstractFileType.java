/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.ResourcePattern;
import com.ryanfuse.crgrep.util.Display;

/**
 * Abstract base class extended by all file type implementations.
 * 
 * File type classes each specialise in handling a particular type of resource.  
 * 
 * This class provides some generic methods for navigating between file type when passing processing of 
 * nested resources from one file type to another. This enables arbitrary combinations of nested
 * resources with each processed by its appropriate file type implementation. 
 *  
 * @author Craig Ryan
 */
public abstract class AbstractFileType {

	public static final String STDIN_STREAM = "<stdin>";
	
    private FileGrep fileGrep;
    
    public AbstractFileType(FileGrep fileGrep) {
        this.fileGrep = fileGrep;
    }

    public FileGrep getFileGrep() {
        return fileGrep;
    }

    /**
     * Determines if the file type supports this file
     * 
     * @param entry the file
     * @return true to accept, false otherwise
     */
    public abstract boolean matchesSupportedType(File entry);
    
    /**
     * For file types which support streams to override
     * 
     * @param entryName the name of the entry in a stream
     * @return
     */
    public boolean matchesSupportedFileName(String entryName) {
        return false;
    }
    
    /**
     * Grep this entry. The method matchesSupportedType(file) is guaranteed to have been called and
     * returned true prior to this call.
     * 
     * @param file the file entry
     */
    public abstract void grepEntry(File file);

    /**
     * Some file types may override if they support streams
     * 
     * @param entryStream stream for the entry
     * @param entryName name (path) for the entry
     * @param entryLabe display name for entryName including parent archive etc
     */
    public void grepEntryStream(InputStream entryStream, String entryName, String entryLabel) {
        return; // no-op
    }
    
    public void grepFileEntry(File file) { 
        grepByName(file.getPath(), file.getName());
        if (fileGrep.isContent()) {
            fileGrep.grepByContents(file);
        }
    }

    public void grepDirEntry(File dir) {
        grepByName(dir.getPath(), dir.getName());
    }
    
    /**
     * Grep an entry listing by name.
     * 
     * @param path the path of the entry
     * @param name the name of the entry (less the path)
     */
    public void grepByName(String path, String name) {
        fileGrep.debug("FileType grep name '" + name + "' for pattern '" + fileGrep.getResourcePattern().getPattern()+"'");
        Matcher matcher = fileGrep.getResourcePattern().getPattern().matcher(name);
        if (matcher.find()) {
            fileGrep.getDisplay().result(path);
        }
    }
    	
    /**
     * Grep plain text file contents.
     * @param file the plain text file.
     */
    public void grepTextContents(File file) {
        try {
        	grepTextContentsStream(new FileInputStream(file), file.getPath());
        } catch (FileNotFoundException e1) {
            ignoreResource(file.getPath(), e1.getLocalizedMessage());
            return;
        }
    }

    /**
     * Grep plain text contents from a stream
     * @param entryStream the entry stream
     * @param entryName the name of the entry
     */
    public void grepTextContentsStream(InputStream entryStream, String entryName) {
        fileGrep.debug("AbstractFileType Grep stream '" + entryName + "' for pattern '" + fileGrep.getResourcePattern().getPattern() + "'");
        BufferedReader br = new BufferedReader(new InputStreamReader(entryStream));
        String line = null;
        try {
            Pattern pattern = getResourcePattern().getPattern();
            int linenum = 1;
            while ((line = br.readLine()) != null) {
                if (pattern.matcher(line).find()) {
                    getDisplay().result(entryName + ":" + linenum + ":" + line);
                }
                linenum++;
            }
        } catch (IOException e) {
            ignoreResource(entryName, e.getLocalizedMessage());
        } 
    }
    
    protected Display getDisplay() {
        return fileGrep.getDisplay();
    }

    protected ResourcePattern getResourcePattern() {
        return fileGrep.getResourcePattern();
    }
    
    protected void ignoreResource(String fname, String err) {
        getDisplay().warn("Ignoring resource '" + fname + "', reason: " + err);
    }
    
    protected String streamEntryName(String entryPath, String entryName) {
    	if (entryPath == null && entryName != null && entryName.equals("-")) {
    		return STDIN_STREAM;
    	}
    	if (entryName == null) {
    		return entryPath;
    	}
    	return entryPath + "[" + entryName + "]";
    }
}
