/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.adobe.AdobeJpegDescriptor;
import com.drew.metadata.adobe.AdobeJpegDirectory;
import com.drew.metadata.exif.CanonMakernoteDescriptor;
import com.drew.metadata.exif.CanonMakernoteDirectory;
import com.drew.metadata.exif.CasioType1MakernoteDescriptor;
import com.drew.metadata.exif.CasioType1MakernoteDirectory;
import com.drew.metadata.exif.CasioType2MakernoteDescriptor;
import com.drew.metadata.exif.CasioType2MakernoteDirectory;
import com.drew.metadata.exif.ExifIFD0Descriptor;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDescriptor;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.ExifThumbnailDescriptor;
import com.drew.metadata.exif.ExifThumbnailDirectory;
import com.drew.metadata.exif.GpsDescriptor;
import com.drew.metadata.exif.GpsDirectory;
import com.drew.metadata.exif.NikonType1MakernoteDescriptor;
import com.drew.metadata.exif.NikonType1MakernoteDirectory;
import com.drew.metadata.exif.NikonType2MakernoteDescriptor;
import com.drew.metadata.exif.NikonType2MakernoteDirectory;
import com.drew.metadata.exif.OlympusMakernoteDescriptor;
import com.drew.metadata.exif.OlympusMakernoteDirectory;
import com.drew.metadata.icc.IccDescriptor;
import com.drew.metadata.icc.IccDirectory;
import com.drew.metadata.iptc.IptcDescriptor;
import com.drew.metadata.iptc.IptcDirectory;
import com.drew.metadata.jfif.JfifDescriptor;
import com.drew.metadata.jfif.JfifDirectory;
import com.drew.metadata.jpeg.JpegCommentDescriptor;
import com.drew.metadata.jpeg.JpegCommentDirectory;
import com.drew.metadata.photoshop.PhotoshopDescriptor;
import com.drew.metadata.photoshop.PhotoshopDirectory;
import com.drew.metadata.xmp.XmpDescriptor;
import com.drew.metadata.xmp.XmpDirectory;
import com.ryanfuse.crgrep.FileGrep;

/**
 * An Image file type to handle and process image meta data.
 * 
 * Images may contain various meta data encoded inside the binary image file.
 * This data typically includes image attributes (size, camera settings etc) and
 * even user defined comments/titles/descriptions and location information.  
 * 
 * Extract any known meta data from the image and include in the grep.
 * 
 * @author Craig Ryan
 */
public class ImageFileType extends AbstractFileType {

    private static final String IMAGE_FILE_EXTENTS = ".*\\.(png|bmp|jpeg|jpg|gif)";
    private static final String META_DELIMITER = ": @";
    
    private String fileName;
    private boolean listedFile;
    private List<String> duplicates = new ArrayList<String>();
    
    public ImageFileType(FileGrep fileGrep) {
        super(fileGrep);
    }

    @Override
    public boolean matchesSupportedType(File entry) {
        boolean isImage = isImageFile(entry.getName());
    	trace("ImageFileType '" + entry.getPath() + "' supported type? isImage=" + isImage);
    	return isImage;
    }
    
    @Override
    public boolean matchesSupportedFileName(String entryName) {
        boolean isImage = isImageFile(entryName);
    	trace("ImageFileType '" + entryName + "' supported stream name? isImage=" + isImage);
        return isImage;
    }
    
    @Override
    public void grepEntry(File file) {
    	fileName = file.getPath();
		try {
	    	Metadata metadata = ImageMetadataReader.readMetadata(file);
	        grepImage(metadata, fileName);
		} catch (ImageProcessingException e) {
            ignoreResource(fileName, e.getLocalizedMessage());
		} catch (IOException e) {
            ignoreResource(fileName, e.getLocalizedMessage());
		}
    }
    
    @Override
    public void grepEntryStream(InputStream entryStream, String entryName, String entryPath) {
    	fileName = streamEntryName(entryPath, entryName);
    	BufferedInputStream bis = entryStream instanceof BufferedInputStream ? (BufferedInputStream) entryStream : new BufferedInputStream(entryStream); 
    	try {
			Metadata metadata = ImageMetadataReader.readMetadata(bis, true);
			grepImage(metadata, entryName);
		} catch (ImageProcessingException e) {
            ignoreResource(entryName, e.getLocalizedMessage());
		} catch (IOException e) {
            ignoreResource(entryName, e.getLocalizedMessage());
		}
    }
    
    /*
     * If its a supported image file type (jpeg etc).
     */
    private boolean isImageFile(String entryName) {
        Pattern archiveExtent = Pattern.compile(IMAGE_FILE_EXTENTS);
        if (archiveExtent.matcher(entryName).matches()) {
//            getFileGrep().debug("File [" + entryName + "] is an image file (by file extent)");
            return true;
        }
        return false;
    }
    
    private void grepImage(Metadata metadata, String entryName) {
    	getFileGrep().debug("ImageFileType grep meta-data for image '" + entryName + "'");
    	if (getFileGrep().getSwitches().isTrace()) {
    		debugDirList(metadata);
    	}
    	duplicates.clear();
    	listedFile = false;
    	// grep various dir types
    	// see http://javadoc.metadata-extractor.googlecode.com/git/2.7.0/index.html
    	if (metadata.containsDirectory(ExifSubIFDDirectory.class)) {
    		grepExifSubIFDDirectory(metadata);
    	}
    	if (metadata.containsDirectory(ExifIFD0Directory.class)) {
    		grepExifIFD0Directory(metadata);
    	}
    	if (metadata.containsDirectory(JpegCommentDirectory.class)) {
    		grepJpegCommentDirectory(metadata);
    	}
    	if (metadata.containsDirectory(JfifDirectory.class)) {
    		grepJfifDirectory(metadata);
    	}
    	if (metadata.containsDirectory(GpsDirectory.class)) {
    		grepGpsDirectory(metadata);
    	}
    	if (metadata.containsDirectory(IccDirectory.class)) {
    		grepIccDirectory(metadata);
    	}
    	if (metadata.containsDirectory(PhotoshopDirectory.class)) {
    		grepPhotoshopDirectory(metadata);
    	}
    	if (metadata.containsDirectory(AdobeJpegDirectory.class)) {
    		grepAdobeJpegDirectory(metadata);
    	}
    	if (metadata.containsDirectory(ExifThumbnailDirectory.class)) {
    		grepExifThumbnailDirectory(metadata);
    	}
    	if (metadata.containsDirectory(XmpDirectory.class)) {
    		grepXmpDirectory(metadata);
    	}
    	if (metadata.containsDirectory(CanonMakernoteDirectory.class)) {
    		grepCanonMakernoteDirectory(metadata);
    	}
    	if (metadata.containsDirectory(OlympusMakernoteDirectory.class)) {
    		grepOlympusMakernoteDirectory(metadata);
    	}
    	if (metadata.containsDirectory(IptcDirectory.class)) {
    		grepIptcDirectory(metadata);
    	}
    	if (metadata.containsDirectory(NikonType1MakernoteDirectory.class)) {
    		grepNikonType1MakernoteDirectory(metadata);
    	}
    	if (metadata.containsDirectory(NikonType2MakernoteDirectory.class)) {
    		grepNikonType2MakernoteDirectory(metadata);
    	}
    	if (metadata.containsDirectory(CasioType1MakernoteDirectory.class)) {
    		grepCasioType1MakernoteDirectory(metadata);
    	}
    	if (metadata.containsDirectory(CasioType2MakernoteDirectory.class)) {
    		grepCasioType2MakernoteDirectory(metadata);
    	}
    }
    
	private void trace(String msg) {
        getFileGrep().trace(msg);
    }

    private void debugDirList(Metadata metadata) {
        Iterable<Directory> dirs = metadata.getDirectories();
        trace("Directory List for [" + fileName + "]");
        trace("-----------------------------------------------");
        StringBuilder sb = new StringBuilder();
        for (Directory d : dirs) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(d.getName());
        }
        trace(sb.toString());
    }

    private void grepCasioType1MakernoteDirectory(Metadata metadata) {
    	CasioType1MakernoteDirectory dir = metadata.getDirectory(CasioType1MakernoteDirectory.class);
    	CasioType1MakernoteDescriptor desc = new CasioType1MakernoteDescriptor(dir);
    	trace("CasioType1Makernote grep>");
    	grepData(new String[] {
   			"CcdSensitivity", desc.getCcdSensitivityDescription(),
			"contrast", desc.getContrastDescription(), 
			"DigitalZoom", desc.getDigitalZoomDescription(), 
			"FlashIntensity", desc.getFlashIntensityDescription(), 
			"FlashMode", desc.getFlashModeDescription(), 
			"FocusingMode", desc.getFocusingModeDescription(), 
			"ObjectDistance", desc.getObjectDistanceDescription(), 
			"Quality", desc.getQualityDescription(), 
			"RecordingMode", desc.getRecordingModeDescription(), 
			"Saturation", desc.getSaturationDescription(), 
			"Sharpness", desc.getSharpnessDescription(), 
			"WhiteBalance", desc.getWhiteBalanceDescription(), 
    	});
	}
    
    private void grepCasioType2MakernoteDirectory(Metadata metadata) {
    	CasioType2MakernoteDirectory dir = metadata.getDirectory(CasioType2MakernoteDirectory.class);
    	CasioType2MakernoteDescriptor desc = new CasioType2MakernoteDescriptor(dir);
    	trace("CasioType2Makernote grep>");
    	// complete
    	grepData(new String[] {
   			"BestShotMode", desc.getBestShotModeDescription(),
			"CasioPreviewThumbnail", desc.getCasioPreviewThumbnailDescription(),
			"CcdSensitivity", desc.getCcdIsoSensitivityDescription(),
			"ColourMode", desc.getColourModeDescription(),
			"Contrast", desc.getContrastDescription(),
			"Enhancement", desc.getEnhancementDescription(),
			"Filter", desc.getFilterDescription(),
			"FlashDistance", desc.getFlashDistanceDescription(),
			"FocalLength", desc.getFocalLengthDescription(),
			"FocusMode1", desc.getFocusMode1Description(),
			"FocusMode2", desc.getFocusMode2Description(),
			"ImageSize", desc.getImageSizeDescription(),
			"IsoSensitivity", desc.getIsoSensitivityDescription(),
			"ObjectDistance", desc.getObjectDistanceDescription(),
			"PrintImageMatchingInfo", desc.getPrintImageMatchingInfoDescription(),
			"Quality", desc.getQualityDescription(),
			"QualityMode", desc.getQualityModeDescription(),
			"RecordMode", desc.getRecordModeDescription(),
			"Saturation", desc.getSaturationDescription(),
			"SelfTimer", desc.getSelfTimerDescription(),
			"Sharpness", desc.getSharpnessDescription(),
			"ThumbnailDimensions", desc.getThumbnailDimensionsDescription(),
			"ThumbnailOffset", desc.getThumbnailOffsetDescription(),
			"ThumbnailSize", desc.getThumbnailSizeDescription(),
			"Timezone", desc.getTimeZoneDescription(),
			"WhiteBalance1", desc.getWhiteBalance1Description(),
			"WhiteBalance2", desc.getWhiteBalance2Description(),
			"WhiteBalanceBias", desc.getWhiteBalanceBiasDescription()
    	});
	}

    private void grepJpegCommentDirectory(Metadata metadata) {
        JpegCommentDirectory dir = metadata.getDirectory(JpegCommentDirectory.class);
        JpegCommentDescriptor desc = new JpegCommentDescriptor(dir);
        trace("JpegComment grep>");
        // complete
        grepData(new String[] {
       		"JpegComment", desc.getJpegCommentDescription()
        });
    }

    private void grepExifIFD0Directory(Metadata metadata) {
        ExifIFD0Directory dir = metadata.getDirectory(ExifIFD0Directory.class);
        ExifIFD0Descriptor desc = new ExifIFD0Descriptor(dir);
        trace("ExifIFDODirectory grep>");
        // complete
        grepData(new String[] {
           	"Orientation", desc.getOrientationDescription(),
	       	"ReferenceBlackWhite", desc.getReferenceBlackWhiteDescription(),
           	"Resolution", desc.getResolutionDescription(),
       		"WindowsAuthor", desc.getWindowsAuthorDescription(), 
           	"WindowsComment", desc.getWindowsCommentDescription(), 
           	"WindowsKeyword", desc.getWindowsKeywordsDescription(), 
           	"WindowsSubject", desc.getWindowsSubjectDescription(),
           	"WindowsTitle", desc.getWindowsTitleDescription(),
           	"XResolution", desc.getXResolutionDescription(),
           	"YcbcrPositioning", desc.getYCbCrPositioningDescription(),
           	"YResolution", desc.getYResolutionDescription()
        });
    }

    private void grepExifSubIFDDirectory(Metadata metadata) {
        ExifSubIFDDirectory directory = metadata.getDirectory(ExifSubIFDDirectory.class);       
        ExifSubIFDDescriptor descriptor = new ExifSubIFDDescriptor(directory);
        //Date date = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
        trace("ExifSubIFDDir grep>");
        // complete
        grepData(new String[] {
        	"Contrast", descriptor.getContrastDescription(),
        	"UserComment", descriptor.getUserCommentDescription(),
			"35mmFilmEquivFocalLength", descriptor.get35mmFilmEquivFocalLengthDescription(),
			"ApertureValue", descriptor.getApertureValueDescription(),
			"BitsPerSample", descriptor.getBitsPerSampleDescription(),
			"ColorSpace", descriptor.getColorSpaceDescription(),
			"ComponentConfiguration", descriptor.getComponentConfigurationDescription(),
			"CompressedAverageBitsPerPixel", descriptor.getCompressedAverageBitsPerPixelDescription(),
			"Contrast", descriptor.getContrastDescription(),
			"CustomRendered", descriptor.getCustomRenderedDescription(),
			"DigitalZoomRatio", descriptor.getDigitalZoomRatioDescription(),
			"ExifImageHeight", descriptor.getExifImageHeightDescription(),
			"ExifImageWidth", descriptor.getExifImageWidthDescription(),
			"ExifVersion", descriptor.getExifVersionDescription(),
			"ExposureBias", descriptor.getExposureBiasDescription(),
			"ExposureMode", descriptor.getExposureModeDescription(),
			"ExposureProgram", descriptor.getExposureProgramDescription(),
			"ExposureTime", descriptor.getExposureTimeDescription(),
			"FileSource", descriptor.getFileSourceDescription(),
			"FillOrder", descriptor.getFillOrderDescription(),
			"Flash", descriptor.getFlashDescription(),
			"FlashPixVersion", descriptor.getFlashPixVersionDescription(),
			"FNumber", descriptor.getFNumberDescription(),
			"FocalLength", descriptor.getFocalLengthDescription(),
			"FocalPlaneResolutionUnit", descriptor.getFocalPlaneResolutionUnitDescription(),
			"FocalPlaneXResolution", descriptor.getFocalPlaneXResolutionDescription(),
			"FocalPlaneYResolution", descriptor.getFocalPlaneYResolutionDescription(),
			"GainControl", descriptor.getGainControlDescription(),
			"IsoEquivalent", descriptor.getIsoEquivalentDescription(),
			"MaxApertureValue", descriptor.getMaxApertureValueDescription(),
			"MeteringMode", descriptor.getMeteringModeDescription(),
			"NewSubfileType", descriptor.getNewSubfileTypeDescription(),
			"PhotometricInterpretation", descriptor.getPhotometricInterpretationDescription(),
			"PlanarConfiguration", descriptor.getPlanarConfigurationDescription(),
			"RowsPerStrip", descriptor.getRowsPerStripDescription(),
			"SamplesPerPixel", descriptor.getSamplesPerPixelDescription(),
			"Saturation", descriptor.getSaturationDescription(),
			"SceneCaptureType", descriptor.getSceneCaptureTypeDescription(),
			"SceneType", descriptor.getSceneTypeDescription(),
			"SensingMethod", descriptor.getSensingMethodDescription(),
			"Sharpness", descriptor.getSharpnessDescription(),
			"ShutterSpeed", descriptor.getShutterSpeedDescription(),
			"StripByteCounts", descriptor.getStripByteCountsDescription(),
			"SubfileType", descriptor.getSubfileTypeDescription(),
			"SubjectDistance", descriptor.getSubjectDistanceDescription(),
			"SubjectDistanceRange", descriptor.getSubjectDistanceRangeDescription(),
			"Thresholding", descriptor.getThresholdingDescription(),
			"UserComment", descriptor.getUserCommentDescription(),
			"WhiteBalance", descriptor.getWhiteBalanceDescription(),
			"WhiteBalanceMode", descriptor.getWhiteBalanceModeDescription(),
			"YCbCrSubsampling", descriptor.getYCbCrSubsamplingDescription()
        });
    }

    private void grepExifThumbnailDirectory(Metadata metadata) {
        ExifThumbnailDirectory directory = metadata.getDirectory(ExifThumbnailDirectory.class);
        ExifThumbnailDescriptor descriptor = new ExifThumbnailDescriptor(directory);
        trace("ExifThumbnailDirectory grep>");
        // complete
        grepData(new String[] {
        	"BitsPerSample", descriptor.getBitsPerSampleDescription(),
        	"Compression", descriptor.getCompressionDescription(),
        	"Orientation", descriptor.getOrientationDescription(),
        	"PhotometricInterpretation", descriptor.getPhotometricInterpretationDescription(),
        	"PlanarConfiguration", descriptor.getPlanarConfigurationDescription(),
        	"ReferenceBlackWhite", descriptor.getReferenceBlackWhiteDescription(),
        	"Resolution", descriptor.getResolutionDescription(),
        	"RowsPerStrip", descriptor.getRowsPerStripDescription(),
        	"SamplesPerPixel", descriptor.getSamplesPerPixelDescription(),
        	"StripByteCounts", descriptor.getStripByteCountsDescription(),
        	"ThumbnailImageHeight", descriptor.getThumbnailImageHeightDescription(),
        	"ThumbnailImageWidth", descriptor.getThumbnailImageWidthDescription(),
        	"ThumbnailLength", descriptor.getThumbnailLengthDescription(),
        	"ThumbnailOffset", descriptor.getThumbnailOffsetDescription(),
        	"XResolution", descriptor.getXResolutionDescription(),
        	"YCbCrPositioning", descriptor.getYCbCrPositioningDescription(),
        	"YCbCrSubsampling", descriptor.getYCbCrSubsamplingDescription(),
        	"YResolution", descriptor.getYResolutionDescription()
		});
    }

    private void grepNikonType1MakernoteDirectory(Metadata metadata) {
        NikonType1MakernoteDirectory directory = metadata.getDirectory(NikonType1MakernoteDirectory.class);
        NikonType1MakernoteDescriptor descriptor = new NikonType1MakernoteDescriptor(directory);
        trace("NikonType1MakernoteDir grep>");
        // complete
        grepData(new String[] {
			"CcdSensitivity", descriptor.getCcdSensitivityDescription(),
			"ColorMode", descriptor.getColorModeDescription(),
			"Converter", descriptor.getConverterDescription(),
			"DigitalZoom", descriptor.getDigitalZoomDescription(),
			"Focus", descriptor.getFocusDescription(),
			"ImageAdjustment", descriptor.getImageAdjustmentDescription(),
			"Quality", descriptor.getQualityDescription(),
			"WhiteBalance", descriptor.getWhiteBalanceDescription()
		});
    }

    private void grepNikonType2MakernoteDirectory(Metadata metadata) {
        NikonType2MakernoteDirectory directory = metadata.getDirectory(NikonType2MakernoteDirectory.class);
        NikonType2MakernoteDescriptor descriptor = new NikonType2MakernoteDescriptor(directory);
        trace("NikonType1MakernoteDir grep>");
        // complete
        grepData(new String[] {
			"ActiveDLighting", descriptor.getActiveDLightingDescription(),
			"AutoFlashCompensation", descriptor.getAutoFlashCompensationDescription(),
			"AutoFocusPosition", descriptor.getAutoFocusPositionDescription(),
			"ColorMode", descriptor.getColorModeDescription(),
			"ColorSpace", descriptor.getColorSpaceDescription(),
			"DigitalZoom", descriptor.getDigitalZoomDescription(),
			"ExposureDifference", descriptor.getExposureDifferenceDescription(),
			"ExposureTuning", descriptor.getExposureTuningDescription(),
			"FirmwareVersion", descriptor.getFirmwareVersionDescription(),
			"FlashBracketCompensation", descriptor.getFlashBracketCompensationDescription(),
			"FlashExposureCompensation", descriptor.getFlashExposureCompensationDescription(),
			"FlashUsed", descriptor.getFlashUsedDescription(),
			"HighISONoiseReduction", descriptor.getHighISONoiseReductionDescription(),
			"HueAdjustment", descriptor.getHueAdjustmentDescription(),
			"IsoSetting", descriptor.getIsoSettingDescription(),
			"Lens", descriptor.getLensDescription(),
			"LensStops", descriptor.getLensStopsDescription(),
			"LensType", descriptor.getLensTypeDescription(),
			"NEFCompression", descriptor.getNEFCompressionDescription(),
			"PowerUpTime", descriptor.getPowerUpTimeDescription(),
			"ProgramShift", descriptor.getProgramShiftDescription(),
			"ShootingMode", descriptor.getShootingModeDescription(),
			"VignetteControl", descriptor.getVignetteControlDescription()
		});
    }

    private void grepIptcDirectory(Metadata metadata) {
        IptcDirectory directory = metadata.getDirectory(IptcDirectory.class);
        IptcDescriptor descriptor = new IptcDescriptor(directory);
        trace("IptcDir grep>");
        // complete
        grepData(new String[] {
			"ByLine", descriptor.getByLineDescription(),
			"ByLineTitle", descriptor.getByLineTitleDescription(),
			"Caption", descriptor.getCaptionDescription(),
			"Category", descriptor.getCategoryDescription(),
			"City", descriptor.getCityDescription(),
			"CopyrightNotice", descriptor.getCopyrightNoticeDescription(),
			"CountryOrPrimaryLocation", descriptor.getCountryOrPrimaryLocationDescription(),
			"Credit", descriptor.getCreditDescription(),
			"DateCreated", descriptor.getDateCreatedDescription(),
			"FileFormat", descriptor.getFileFormatDescription(),
			"Headline", descriptor.getHeadlineDescription(),
			"Keywords", descriptor.getKeywordsDescription(),
			"ObjectName", descriptor.getObjectNameDescription(),
			"OriginalTransmissionReference", descriptor.getOriginalTransmissionReferenceDescription(),
			"OriginatingProgram", descriptor.getOriginatingProgramDescription(),
			"ProvinceOrState", descriptor.getProvinceOrStateDescription(),
			"RecordVersion", descriptor.getRecordVersionDescription(),
			"ReleaseDate", descriptor.getReleaseDateDescription(),
			"ReleaseTime", descriptor.getReleaseTimeDescription(),
			"Source", descriptor.getSourceDescription(),
			"SpecialInstructions", descriptor.getSpecialInstructionsDescription(),
			"SupplementalCategories", descriptor.getSupplementalCategoriesDescription(),
			"TimeCreated", descriptor.getTimeCreatedDescription(),
			"Urgency", descriptor.getUrgencyDescription(),
			"Writer", descriptor.getWriterDescription()
		});
    }

    private void grepOlympusMakernoteDirectory(Metadata metadata) {
        OlympusMakernoteDirectory directory = metadata.getDirectory(OlympusMakernoteDirectory.class);
        OlympusMakernoteDescriptor descriptor = new OlympusMakernoteDescriptor(directory);
        trace("OlympusMakernoteDir grep>");
        // complete
        grepData(new String[] {
			"MacroMode", descriptor.getMacroModeDescription(),
			"DigiZoomRatio", descriptor.getDigiZoomRatioDescription(),
			"JpegQuality", descriptor.getJpegQualityDescription(),
			"SpecialMode", descriptor.getSpecialModeDescription()
		});
    }

    private void grepXmpDirectory(Metadata metadata) {
        XmpDirectory directory = metadata.getDirectory(XmpDirectory.class);
        XmpDescriptor descriptor = new XmpDescriptor(directory);
        trace("XmpDir grep>");
        // complete
        grepData(new String[] {
			"ApertureValue", descriptor.getApertureValueDescription(),
			"ExposureProg", descriptor.getExposureProgramDescription(),
			"ExposureTime", descriptor.getExposureTimeDescription(),
			"FNumber", descriptor.getFNumberDescription(),
			"FocalLength", descriptor.getFocalLengthDescription(),
			"ShutterSpeed", descriptor.getShutterSpeedDescription()
		});
    }

    private void grepCanonMakernoteDirectory(Metadata metadata) {
        CanonMakernoteDirectory directory = metadata.getDirectory(CanonMakernoteDirectory.class);
        CanonMakernoteDescriptor descriptor = new CanonMakernoteDescriptor(directory);
        trace("CanonMakernoteDir grep>");
        // complete
        grepData(new String[] {
			"AfPointSelected", descriptor.getAfPointSelectedDescription(),
			"AfPointUsed", descriptor.getAfPointUsedDescription(),
			"ContinuousDriveMode", descriptor.getContinuousDriveModeDescription(),
			"Contrast", descriptor.getContrastDescription(),
			"DigitalZoom", descriptor.getDigitalZoomDescription(),
			"EasyShootingMode", descriptor.getEasyShootingModeDescription(),
			"ExposureMode", descriptor.getExposureModeDescription(),
			"FlashActivity", descriptor.getFlashActivityDescription(),
			"FlashBias", descriptor.getFlashBiasDescription(),
			"FlashDetails", descriptor.getFlashDetailsDescription(),
			"FlashMode", descriptor.getFlashModeDescription(),
			"FlashMode", descriptor.getFlashModeDescription(),
			"FocalUnitsPerMillimetre", descriptor.getFocalUnitsPerMillimetreDescription(),
			"FocusMode1", descriptor.getFocusMode1Description(),
			"FocusMode2", descriptor.getFocusMode2Description(),
			"FocusType", descriptor.getFocusTypeDescription(),
			"ImageSize", descriptor.getImageSizeDescription(),
			"LongFocalLength", descriptor.getLongFocalLengthDescription(),
			"MacroMode", descriptor.getMacroModeDescription(),
			"MeteringMode", descriptor.getMeteringModeDescription(),
			"Quality", descriptor.getQualityDescription(),
			"Saturation", descriptor.getSaturationDescription(),
			"SelfTimerDelay", descriptor.getSelfTimerDelayDescription(),
			"SerialNumber", descriptor.getSerialNumberDescription(),
			"Sharpness", descriptor.getSharpnessDescription(),
			"ShortFocalLength", descriptor.getShortFocalLengthDescription(),
			"WhiteBalance", descriptor.getWhiteBalanceDescription()
		});
    }

    private void grepAdobeJpegDirectory(Metadata metadata) {
        AdobeJpegDirectory directory = metadata.getDirectory(AdobeJpegDirectory.class);
        AdobeJpegDescriptor descriptor = new AdobeJpegDescriptor(directory);
        /* nothing to grep
        String comment = descriptor.toString(); 
        debug("AdobeJpegDir grep>");
        grepData(new String[] {comment});
        */
    }

    private void grepPhotoshopDirectory(Metadata metadata) {
        PhotoshopDirectory directory = metadata.getDirectory(PhotoshopDirectory.class);
        PhotoshopDescriptor descriptor = new PhotoshopDescriptor(directory);
        trace("PhotoshopDir grep>");
        String q = "";
        try {
			q = descriptor.getDescription(PhotoshopDirectory.TAG_PHOTOSHOP_JPEG_QUALITY);
		} catch (Exception e) {}
        String p = "";
        try {
        	p = descriptor.getPixelAspectRatioString();
		} catch (Exception e) {}
        // complete
        grepData(new String[] {
			"JpegQuality", q,
			"PixelAspectRatio", p,
			"PrintScale", descriptor.getPrintScaleDescription(),
			"ResolutionInfo", descriptor.getResolutionInfoDescription(),
			"Slices", descriptor.getSlicesDescription(),
			"Version", descriptor.getVersionDescription()
		});
    }

    private void grepIccDirectory(Metadata metadata) {
        IccDirectory directory = metadata.getDirectory(IccDirectory.class);
        IccDescriptor descriptor = new IccDescriptor(directory);
        // complete
        /* nothing to grep
        String comment = descriptor.toString(); // nothing to grep
        debug("IccDir grep>");
        grepData(new String[] {comment});
        */
    }

    private void grepGpsDirectory(Metadata metadata) {
        GpsDirectory directory = metadata.getDirectory(GpsDirectory.class);
        GpsDescriptor descriptor = new GpsDescriptor(directory);
        trace("GpsDir grep>");
        // complete
        grepData(new String[] {
			"DegreesMinutesSeconds", descriptor.getDegreesMinutesSecondsDescription(),
			"GpsAltitude", descriptor.getGpsAltitudeDescription(),
			"GpsAltitudeRef", descriptor.getGpsAltitudeRefDescription(),
			"GpsDestinationReference", descriptor.getGpsDestinationReferenceDescription(),
			"GpsDifferential", descriptor.getGpsDifferentialDescription(),
			"GpsLatitude", descriptor.getGpsLatitudeDescription(),
			"GpsLongitude", descriptor.getGpsLongitudeDescription(),
			"GpsMeasureMode", descriptor.getGpsMeasureModeDescription(),
			"GpsSpeedRef", descriptor.getGpsSpeedRefDescription(),
			"GpsStatus", descriptor.getGpsStatusDescription(),
			"GpsTimeStamp", descriptor.getGpsTimeStampDescription()
		});
    }

    private void grepJfifDirectory(Metadata metadata) {
        JfifDirectory directory = metadata.getDirectory(JfifDirectory.class);
        JfifDescriptor descriptor = new JfifDescriptor(directory);
        trace("JfifDir grep>");
        // complete
        grepData(new String[] {
			"ImageResUnits", descriptor.getImageResUnitsDescription(),
			"ImageResX", descriptor.getImageResXDescription(),
			"ImageResY", descriptor.getImageResYDescription(),
			"ImageVersion", descriptor.getImageVersionDescription()
		});
    }

    private void grepData(String[] meta) {
        Pattern pattern = getFileGrep().getResourcePattern().getPattern();
        if (!getFileGrep().isContent()) {
        	if (listedFile) {
        		// Already listed the file after a match. Avoid duplicate reporting.
        		return;
        	}
        }
        StringBuilder sb = new StringBuilder();
        boolean haveMatch = false;
        for (int i = 0; i < meta.length; i += 2) {
            String label = meta[i];
            String data = meta[i+1];
            if (getFileGrep().getSwitches().isTrace()) {
            	trace("    -> label [" + label + "] data [" + data + "]] against [" + getResourcePattern().getPattern() + "]");
            }
            if (data == null || data.trim().length() == 0) {
                continue;
            }
            String key = label + META_DELIMITER + data;
            if (duplicates.contains(key)) {
            	// Avoid reporting on duplicate data from different Directory types for this image.
            	trace("Ignore duplicate label '" + label + "' data '" + data + "'");
            	continue;
            }
            duplicates.add(key);
            if (pattern.matcher(data).find()) {
                if (sb.length() == 0) {
                    sb.append("{");
                } else {
                    sb.append(", ");
                }
                sb.append(label).append("=").append(data);
                haveMatch = true;
            }
        }

        if (haveMatch) {
            sb.append("}");
            getDisplay().result(fileName + (getFileGrep().isContent() ? META_DELIMITER + sb.toString() : ""));
            listedFile = true;
        }
    }

}
