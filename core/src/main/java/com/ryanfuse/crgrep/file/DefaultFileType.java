/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

import com.ryanfuse.crgrep.FileGrep;

/**
 * Catch-all file type called LAST by the FileTypeFactory if no other file type accepts
 * the file.
 * 
 * This file type handles both Files and InputStreams and determines if the content
 * is plain text in either case. Binary content is ignored.
 * 
 * @author Craig Ryan
 */
public class DefaultFileType extends AbstractFileType {

    public DefaultFileType(FileGrep fileGrep) {
        super(fileGrep);
    }

    @Override
    public boolean matchesSupportedType(File entry) {
        return true;
    }

    @Override
    public boolean matchesSupportedFileName(String entryName) {
        return true;
    }
    
    @Override
    public void grepEntry(File file) {
    	getFileGrep().debug("DefaultFileType grep file '" + file.getPath() + "'");
    	grepByName(file.getPath(), file.getName());
    	if (!getFileGrep().isContent()) {
    		return;
    	}
        try {
            if (!getFileGrep().isBinaryFile(file)) {
                grepTextContents(file);
            } else {
                ignoreResource(file.getPath(), "unsupported binary file type");
            }
        } catch (FileNotFoundException e) {
            ignoreResource(file.getPath(), e.getLocalizedMessage());
        } catch (IOException e) {
            ignoreResource(file.getPath(), e.getLocalizedMessage());
        }
    }
    
    /**
     * Check for binary data in the stream, if not binary then grep the content as text 
     */
    @Override
    public void grepEntryStream(InputStream entryStream, String entryName, String entryPath) {
        byte[] data = new byte[512 /*size*/];
        int length = 0;
        String fullName = streamEntryName(entryPath, entryName);
        try {
            length = entryStream.read(data);
			getFileGrep().debug("DefaultFileType Grep stream " + fullName + "' bytes read " + length + " mark supported " + entryStream.markSupported());
        } catch (IOException e) {
            ignoreResource(fullName, e.getLocalizedMessage());
            return;
        }
        if (length <= 0) {
            ignoreResource(fullName, "empty data stream");
            return;
        }
        if (getFileGrep().isBinaryData(data, length)) {
            ignoreResource(fullName, "unsupported binary data stream");
            return; // don't attempt to read binary files
        }
        PushbackInputStream pbis = new PushbackInputStream(entryStream, length);
        try {
            pbis.unread(data, 0, length);
        } catch (IOException e) {
            ignoreResource(fullName, e.getLocalizedMessage());
            return; 
        }
        grepTextContentsStream(pbis, fullName);
    }
}
