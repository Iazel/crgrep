/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file.pom;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.ArtifactRepositoryPolicy;
import org.apache.maven.artifact.repository.MavenArtifactRepository;
import org.apache.maven.artifact.repository.layout.DefaultRepositoryLayout;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.sonatype.aether.artifact.Artifact;
import org.sonatype.aether.resolution.DependencyResolutionException;
import org.sonatype.aether.util.artifact.DefaultArtifact;
import org.sonatype.aether.util.artifact.JavaScopes;

import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.Switches;

/**
 * Navigate a maven POM dependency list looking for resources (artifacts) to include in the search.
 * 
 * TODO: could define an option to follow <modules> and process those POM files also 
 * 
 * @author Craig Ryan
 */
public class PomParser {
    
    private static final String DEFAULT_REMOTE_REPO = "http://repo1.maven.org/maven2/";
    private static final String POM_FILE_PREFIX = "->";
    private List<File> artifacts = new ArrayList<File>();
    private String remoteRepoList;  // syntax is 'urlRepo1;urlRepo2;..'
    private List<ArtifactRepository> remoteRepos = new ArrayList<ArtifactRepository>(); 
    private File localRepoFolder;
    
    private File projectPom;
    private Display display;
    private String prefixName;
    
    public PomParser(Switches switches, Display display) {
        this.display = display;
        Properties userProperties = switches.getUserProperties();
        if (userProperties != null) {
        	setRemoteRepoList(userProperties.getProperty("maven.remoteRepos"));
        	setLocalRepo(userProperties.getProperty("maven.localRepo"));
        } else {
        	setRemoteRepoList(null);
        	setLocalRepo(null);
        }
    }
    
    /**
     * Main entry point to initiate processing of a new POM file.
     * 
     * Extract all artifacts and store as a list of File resources.
     * 
     * @param pomFile the project POM file 
     */
    public void parsePomFile(File pomFile) {
        this.projectPom = pomFile;
        prefixName = projectPom.getPath();
        display.debug("PomParser parse file '" + pomFile.getPath() + "'");
        reset();
        try {
            saveArtifactsFromMavenProjectFile();
        } catch (DependencyResolutionException e) {
            display.warn("POM file '" + pomFile.getPath() + "' failed parsing with Dependency Resolution error, cause: " + e.getLocalizedMessage());
        } catch (IOException e) {
            display.warn("POM file '" + pomFile.getPath() + "' failed parsing with IO error, cause: " + e.getLocalizedMessage());
        } catch (XmlPullParserException e) {
            display.warn("POM file '" + pomFile.getPath() + "' failed parsing with XML error, cause: " + e.getLocalizedMessage());
        }
    }

    /**
     * Alternative to {@link #parsePomFile(File)} which processes a POM as an input stream.
     * 
     * @param pomStream POM stream
     * @param pomFileName name of POM file associated with this stream
     */
    public void parsePomFileStream(InputStream pomStream, String pomFileName) {
        reset();
        prefixName = pomFileName;
        try {
            saveArtifactsFromMavenProjectStream(pomStream);
        } catch (DependencyResolutionException e) {
            display.warn("POM stream '" + pomFileName + "' failed parsing with Dependency Resolution error, cause: " + e.getLocalizedMessage());
        } catch (IOException e) {
            display.warn("POM stream '" + pomFileName + "' failed parsing with IO error, cause: " + e.getLocalizedMessage());
        } catch (XmlPullParserException e) {
            display.warn("POM stream '" + pomFileName + "' failed parsing with XML error, cause: " + e.getLocalizedMessage());
        }
    }

    /**
     * Get POM file name prefix for displaying results. This includes the POM file name
     * plus a delimiter.
     * 
     * @return the name and delimiter string. 
     */
    public String getPrefix() {
        return prefixName + POM_FILE_PREFIX;
    }
    
    /**
     * Get a list of artifacts (resources) from the current POM. 
     * 
     * @return a list (possibly empty) of artifacts
     */
    public List<File> getDependencyList() {
        return artifacts;
    }

    /*
     * Process the POM and extract artifacts (resources) that will be included in the search list.
     */
    private void saveArtifactsFromMavenProjectFile() throws DependencyResolutionException, IOException, XmlPullParserException {
        MavenProject proj = readMavenProject(projectPom);
        processMavenProject(proj);
    }
    
    private void saveArtifactsFromMavenProjectStream(InputStream pomStream) throws DependencyResolutionException, IOException, XmlPullParserException {
        MavenProject proj = readMavenProjectFromStream(pomStream);
        processMavenProject(proj);
    }
    
    /*
     * Navigate the POM tree extracting dependency artifact paths
     */
    private void processMavenProject(MavenProject proj) throws DependencyResolutionException, IOException, XmlPullParserException {
    	List<Dependency> dependencyList = proj.getDependencies();
    	Iterator<Dependency> dependencies = dependencyList.iterator();
    	Properties properties = proj.getProperties();
    	Map<String, String> propMap = new HashMap(properties);
    	proj.setRemoteArtifactRepositories(remoteRepos);
        PomResolver pomResolver = new PomResolver(proj, localRepoFolder, display);
        while (dependencies.hasNext()) {
    		Dependency dependency = dependencies.next();
    		String scope = dependency.getScope();
    		if (scope == null) {
    			// default is COMPILE if <scope> isn't specified. 
    			scope = JavaScopes.SYSTEM;
    		}
    		String grpId = sub(dependency.getGroupId(), properties);
    		String artId = sub(dependency.getArtifactId(), properties);
    		String classifier = sub(dependency.getClassifier(), properties);
    		String typ = sub(dependency.getType(), properties);
    		String ver = sub(dependency.getVersion(), properties);
    		display.debug("Resolving dependency Grp[" + grpId + "] Artifact[" + artId + "] Scope[" + scope + "] Ver[" + ver + "]");
    		Artifact root = new DefaultArtifact(
    				grpId, 
    				artId, 
    				classifier, 
    				typ,
    				ver, 
    				propMap, 
    				(File)null);
    		Collection<Artifact> artifactList = pomResolver.resolve(
    				root,
    				scope
    				);
    		 Iterator<Artifact> artifactIter = artifactList.iterator();
             while (artifactIter.hasNext()) {
                 Artifact artifact = artifactIter.next();
                 artifacts.add(artifact.getFile());
             }
        }
    }

    /*
     * Substitute variables in dependency data
     */
    private String sub(String v, Properties p) {
        if (v == null) {
            return v;
        }
        int i = v.indexOf("${");
        while (i != -1) {
            int j = v.indexOf("}");
            if (j < i) {
                break; // invalid format
            }
            String var = v.substring(i + 2, j);
            String val = p.getProperty(var);
            if (val != null) {
                String re = Pattern.quote("${" + var + "}");
                v = v.replaceAll(re, val);
            }
            i = v.indexOf("${");
        }
        return v;
    }

    /*
     * Read the POM into a MavenProject container
     */
    private MavenProject readMavenProject(File pomFile) throws IOException, XmlPullParserException {
        MavenXpp3Reader mavenReader = new MavenXpp3Reader();
        MavenProject ret = null;
        if (pomFile != null && pomFile.exists()) {
            FileReader reader = null;
            try {
                reader = new FileReader(pomFile);
                Model model = mavenReader.read(reader);
                model.setPomFile(pomFile);
                ret = new MavenProject(model);
            } finally {
                reader.close();
            }
        }
        return ret;
    }

    private MavenProject readMavenProjectFromStream(InputStream pomFile) throws IOException, XmlPullParserException {
        MavenXpp3Reader mavenReader = new MavenXpp3Reader();
        MavenProject ret = null;
        if (pomFile != null) {
            try {
                Model model = mavenReader.read(pomFile);
                ret = new MavenProject(model);
            } finally {
            }
        }
        return ret;
    }
    
    /*
     * Setup a list of remote repos, either configured or default
     */
    private void setRemoteRepoList(String repoList) {
        this.remoteRepoList = repoList == null ? DEFAULT_REMOTE_REPO : repoList;
        String[] dirs = remoteRepoList.split(";");
        for (String dir : dirs) {
            try {
                new URL(dir); // valid url, should be OK
                String id = dir;
                if (dir.equals(DEFAULT_REMOTE_REPO)) {
                    id = "maven-central";
                }
                remoteRepos.add(new MavenArtifactRepository(
                        id,
                        dir,
                        new DefaultRepositoryLayout(),
                        new ArtifactRepositoryPolicy(), 
                        new ArtifactRepositoryPolicy()
                        ));
                display.debug("Include remote repo [" + dir + "]");
            } catch (MalformedURLException e) {
                display.warn("Maven remote repo path '" + dir + "' is invalid. Ignored.");
                // ignore
            }
        }
    }

    /*
     * Setup the local repo path from config or default.
     */
    private void setLocalRepo(String locRepo) {
        File repoFile;
        if (locRepo != null) {
            repoFile = new File(locRepo);
            if (repoFile.exists() && repoFile.isDirectory()) {
                localRepoFolder = repoFile;
                display.debug("Include local repo [" + locRepo + "]");
                return;
            }
            display.warn("Maven local repo path '" + locRepo + "' invalid. Ignored.");
            // fall-through, try the default..
        } 
        String home = System.getProperty("user.home");
        locRepo = home + File.separator + ".m2" + File.separator + "repository/";
        repoFile = new File(locRepo);
        if (repoFile.exists() && repoFile.isDirectory()) {
            localRepoFolder = repoFile;
            display.debug("Include local repo [" + locRepo + "]");
        }
    }

    /*
     * Clear the artifacts from the last POM. Leave startup config unchanged.
     */
    private void reset() {
        artifacts.clear();
    }
}
