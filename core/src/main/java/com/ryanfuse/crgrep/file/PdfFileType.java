package com.ryanfuse.crgrep.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.regex.Pattern;

import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.BadSecurityHandlerException;
import org.apache.pdfbox.pdmodel.encryption.StandardDecryptionMaterial;
import org.apache.pdfbox.util.PDFTextStripper;

import com.ryanfuse.crgrep.FileGrep;

/**
 * File type for grep'ing PDF documents.
 * 
 * @author Craig Ryan
 */
public class PdfFileType extends AbstractFileType {

	private static final String PDF_EXTENT = ".pdf";

	public PdfFileType(FileGrep fileGrep) {
		super(fileGrep);
	}

	@Override
	public boolean matchesSupportedType(File entry) {
		return entry.getName().toLowerCase().endsWith(PDF_EXTENT);
	}

	@Override
	public void grepEntry(File file) {
		try {
			grepEntryStream(new FileInputStream(file), file.getPath(), "");
		} catch (FileNotFoundException e) {
			ignoreResource(file.getPath(), e.getLocalizedMessage());
		} 
	}

	public void grepEntryStream(InputStream entryStream, String entryName, String entryLabel) {
		PDDocument doc = null;
		try {
			doc = PDDocument.load(entryStream);
			if (!documentIsDecrypted(doc)) {
				ignoreResource(entryName, "Unable to decrypt file (is -p password correct?)");
			}
			int pages = doc.getNumberOfPages();
			getDisplay().debug("PDF file '" + entryName + "' number of pages: " + pages);
			PDFTextStripper ps = new PDFTextStripper();
			for (int page = 1; page <= pages; page++){
				grepPdfPage(entryName, doc, page, ps);
			}
		} catch (IOException e) {
			ignoreResource(entryName, e.getLocalizedMessage());
		} finally {
			if (doc != null) {
				try {
					doc.close();
				} catch (IOException e) {
					// ignore
				}
			}
		}
    }

    private boolean documentIsDecrypted(PDDocument doc) {
    	if (!doc.isEncrypted()) {
    		getDisplay().debug("PDF file is not encrypted");
    		return true;
    	}
    	String password = getFileGrep().getPw();
    	if (password == null) {
    		password = "";
    	}
		getDisplay().debug("PDF file is encrypted, attempted to decrypt.");
    	StandardDecryptionMaterial sdm = new StandardDecryptionMaterial(password);
        try {
			doc.openProtection(sdm);
		} catch (BadSecurityHandlerException e) {
    		return false;
		} catch (IOException e) {
    		return false;
		} catch (CryptographyException e) {
    		return false;
		}
        AccessPermission ap = doc.getCurrentAccessPermission();
        return ap.canExtractContent();
	}

	private void grepPdfPage(String entryName, PDDocument doc, int page, PDFTextStripper ps) throws IOException {
		getDisplay().trace("PDF file '" + entryName + "' grep page: " + page);
		ps.setStartPage(page);
		ps.setEndPage(page);
		StringWriter sw = new StringWriter();
		ps.writeText(doc, sw);
		BufferedReader br = new BufferedReader(new StringReader(sw.toString()));
		String line = null;
		Pattern pattern = getResourcePattern().getPattern();
		int linenum = 1;
		while ((line = br.readLine()) != null) {
			if (pattern.matcher(line).find()) {
				// report as <name>:<page 
				getDisplay().result(entryName + ":" + page + ":" + linenum + ":" + line);
			}
			linenum++;
		}
	}
}
