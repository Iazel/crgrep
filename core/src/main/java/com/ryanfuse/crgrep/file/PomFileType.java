/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.file;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import com.ryanfuse.crgrep.FileGrep;
import com.ryanfuse.crgrep.file.pom.PomParser;

/**
 * A Project Object Model (maven POM) file type implementation.
 * 
 * A POM may be discovered in the search list or inside another resource
 * (say within a JAR artifact). If -m is enabled then we will extract
 * artifact dependencies from the POM and include those in the search list.
 *  
 * @author Craig Ryan
 */
public class PomFileType extends AbstractFileType {

    private static final String POM_FILE = "pom.xml";
    private static final int MAX_DEFAULT_DEPTH = 1;
    
    private PomParser pomParser;
    private int depth;
    private int maxDepth = MAX_DEFAULT_DEPTH;

    public PomFileType(FileGrep fileGrep) {
        super(fileGrep);
        depth = 0;
        Properties userProperties = getFileGrep().getSwitches().getUserProperties();
        if (userProperties != null) {
            String max = userProperties.getProperty("maven.depth");
            try {
                maxDepth = Integer.valueOf(max);
            } catch (NumberFormatException e) {
            	getDisplay().warn("User property 'maven.depth' in HOME/.crgrep with value " + max + " can't be converted to integer. Defaulting to 1");
                // ignore
            }
        }
    }

    /**
     * Accept the POM file for parsing if its a valid '*pom.xml' name and 
     * -m (maven) option is enabled. POMs are not processed by default.
     */
    @Override
    public boolean matchesSupportedType(File entry) {
        boolean matches = isPomFile(entry.getName()) && getFileGrep().isMaven();
    	getFileGrep().trace("PomFileType '" + entry.getPath() + "' supported type? " + (matches ? "true" : " false" + (getFileGrep().isMaven() ? "(ignoreMaven)" : "(not POM)")));
    	return matches;
    }

    @Override
    public boolean matchesSupportedFileName(String entryName) {
        boolean matches = isPomFile(entryName) && getFileGrep().isMaven();
    	getFileGrep().trace("PomFileType '" + entryName + "' supported type? " + (matches ? "true" : " false" + (getFileGrep().isMaven() ? "(ignoreMaven)" : "(not POM)")));
    	return matches;
    }
    
    /**
     * Grep a POM file. The depth limit is enforced to prevent unlimited nested, 
     * for example a POM file which contains a JAR artifact which in turn contains
     * a POM file and so on, which could potentially lead to cycles. 
     */
    @Override
    public void grepEntry(File file) {
    	if (!withinDepth(file.getPath())) {
    		return;
    	}
        try {
        	getFileGrep().debug("PomFileType grep entry '" + file.getPath() + "' (depth now " + depth + " of max " + maxDepth + ")");
            grepPomEntry(file);
        } finally {
            depth--;
        }
    }

    @Override
    public void grepEntryStream(InputStream entryStream, String entryName, String entryPath) {
    	String fullName = streamEntryName(entryPath, entryName);
    	if (!withinDepth(fullName)) {
    		return;
    	}
        try {
        	getFileGrep().debug("PomFileType grep entry '" + fullName + "' (depth now " + depth + " of max " + maxDepth + ")");
            grepPomEntryStream(entryStream, entryName, entryPath);
        } finally {
            depth--;
        }
    }

    private boolean withinDepth(String entryName) {
        if (depth + 1 > maxDepth) {
        	getFileGrep().trace("PomFileType grep entry '" + entryName + "' ignored, reached maxDepth of " + maxDepth);
            return false;
        }
        ++depth;
        return true;
    }
    
    private boolean isPomFile(String fileName) {
        return fileName.endsWith(POM_FILE);
    }
    
    /*
     * Parse the POM and extract artifacts and include in the search path
     */
    private void grepPomEntry(File pomFile) {
        grepByName(pomFile.getPath(), pomFile.getName());
        createParser();
        pomParser.parsePomFile(pomFile);
        grepDependencyList();
    }

    /*
     * Parse the POM from a stream and extract artifacts and include in the search path
     */
    private void grepPomEntryStream(InputStream entryStream, String entryName, String entryPath) {
    	String fullPath = streamEntryName(entryPath, entryName);
//    	grepByName(fullPath, entryName);
        createParser();
        pomParser.parsePomFileStream(entryStream, fullPath);
        grepDependencyList();
    }

    /*
     * Grep the dependency list extracted from the POM
     */
    private void grepDependencyList() {
        List<File> depList = pomParser.getDependencyList();
        if (depList.isEmpty()) {
            return;
        }
        getDisplay().setPrefix(pomParser.getPrefix());
        for (File artifact : depList) {
            grepFileEntry(artifact);
        }
        getDisplay().unsetPrefix();

    }
	private void createParser() {
		if (pomParser == null) {
            pomParser = new PomParser(getFileGrep().getSwitches(), getDisplay());
        }
	}
}
