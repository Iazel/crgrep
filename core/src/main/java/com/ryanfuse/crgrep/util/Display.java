/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.io.PrintStream;

/**
 * Class to handle all output displayed
 * 
 * @author Craig Ryan
 */
public class Display {
	private boolean debugOn;
	private boolean traceOn;
	private boolean warnOn;
	// Prefix for log messages
    private String logPrefix = null;
	// Prefix for displaying results
    private String prefix = null;
	
	/*
	 * Display a result (grep match)
	 */
	public Display result(String msg) {
	    if (getPrefix() != null) {
	        System.out.print(getPrefix());
	    }
	    System.out.println(msg);
		return this;
	}

	public String getPrefix() {
        return prefix;
    }

    public void unsetPrefix() {
        this.prefix = null;
    }
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Display warn(String msg) {
    	if (isWarnOn()) {
    		System.err.println(msg);
    	}
		return this;
	}

	public Display error(String msg) {
		System.err.println(msg);
		return this;
	}

	public Display trace(String msg) {
		if (isTraceOn()) {
			prefix(System.err);
			System.err.println(msg);
		}
		return this;
	}

	public Display debug(String msg) {
		if (isDebugOn()) {
			prefix(System.err);
			System.err.println(msg);
		}
		return this;
	}

	private Display prefix(PrintStream ps) {
		if (getLogPrefix() == null) {
			return this;
		}
		ps.print("(" + getLogPrefix() + ") ");
		return this;
	}

	public String getLogPrefix() {
		return logPrefix;
	}

	/**
	 * Set a prefix
	 * @param prefix
	 */
	public void setLogPrefix(String prefix) {
		this.logPrefix = prefix;
	}

	public boolean isDebugOn() {
		return debugOn;
	}

	public void setDebugOn(boolean debugOn) {
		this.debugOn = debugOn;
	}

	public boolean isTraceOn() {
		return traceOn;
	}

	public void setTraceOn(boolean traceOn) {
		this.traceOn = traceOn;
	}

	public boolean isWarnOn() {
		return warnOn;
	}

	public void setWarnOn(boolean warnOn) {
		this.warnOn = warnOn;
	}
}
