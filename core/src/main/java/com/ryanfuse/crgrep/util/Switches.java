/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep.util;

import java.util.Properties;

import org.apache.commons.cli.CommandLine;

/**
 * Switches (toggles) specified by the command line option list. Also holds a reference
 * to Properties loaded from ~/.crgrep.
 * 
 * @author Craig Ryan
 */
public class Switches {

    private boolean verbose;
    private boolean debug;
    private boolean trace;

    private boolean recurse;
    private boolean content;
    private boolean allcontent;
    private boolean listing;
    private boolean database;
    private boolean maven;
    private Properties userProperies;

    public Switches() {
    }

    public Switches(CommandLine line) {
        verbose = line.hasOption('v');
        recurse = line.hasOption('r');
        listing = true;
        content = !line.hasOption('l');
        maven = line.hasOption('m');
        allcontent = line.hasOption('a');
        if (!content && allcontent) {
            content = true;
        }
        database = line.hasOption('d');
        // exclusions - TODO
        /*
        if (line.hasOption('e')) {
            String[] exs = line.getOptionValues('e');
            for (String ex : exs) {
                if (ex.equals("l")) {
                    listing = false;
                }
            }
        }
        */
    }
    
    /**
     * Extensions such as debug, trace
     *  
     * @param extensions
     */
    public void processExtensions(String extensions) {
        String[] exs = extensions.split(",");
        for (String extension : exs) {
            if (extension.equals("debug")) {
                debug = true;
            } else if (extension.equals("trace")) {
                trace = true;
            }
        }
    }

    /**
     * Verbose output?
     * @return
     */
    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    /**
     * Debug output?  
     * 
     * @return
     */
    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    /**
     * Trace output?
     * @return
     */
    public boolean isTrace() {
        return trace;
    }

    public void setTrace(boolean trace) {
        this.trace = trace;
    }

    /**
     * Recurse into sub directories?
     * 
     * @return
     */
    public boolean isRecurse() {
        return recurse;
    }

    public void setRecurse(boolean recurse) {
        this.recurse = recurse;
    }

    /**
     * Include content in search (instead of just resource name matches)
     *  
     * @return
     */
    public boolean isContent() {
        return content;
    }

    public void setContent(boolean content) {
        this.content = content;
    }

    /**
     * Include all content types in search rather than just text related resources? 
     * 
     * @return
     */
    public boolean isAllcontent() {
        return allcontent;
    }

    public void setAllcontent(boolean allcontent) {
        this.allcontent = allcontent;
    }

    /**
     * Are Listings included in the search?
     * 
     * @return
     */
    public boolean isListing() {
        return listing;
    }

    public void setListing(boolean listing) {
        this.listing = listing;
    }

    /**
     * Is this a Database search?
     * 
     * @return
     */
    public boolean isDatabase() {
        return database;
    }

    public void setDatabase(boolean database) {
        this.database = database;
    }

    /**
     * Should the search expand POM file(s) and include those dependencies (artifacts) in the search? 
     * @return
     */
    public boolean isMaven() {
        return maven;
    }

    public void setMaven(boolean maven) {
        this.maven = maven;
    }

    /**
     * Store a reference to the user properties (~/.crgrep) as a convenience in here for 
     * any implementation classes to lookup their own properties.
     * 
     * @param userProperties
     */
    public void setUserProperties(Properties userProperties) {
        this.userProperies = userProperties;
    }

    public Properties getUserProperties() {
        return userProperies;
    }
}
