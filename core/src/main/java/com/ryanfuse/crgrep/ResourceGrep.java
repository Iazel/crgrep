/*
 * (C) Copyright 2013 Craig Ryan. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * (LGPL) version 2.1 which accompanies this distribution, and is 
 * available at http://www.gnu.org/licenses/lgpl-2.1.html
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * @author Craig Ryan
 */
package com.ryanfuse.crgrep;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;

import com.ryanfuse.crgrep.util.Display;
import com.ryanfuse.crgrep.util.Switches;


/**
 * Common Resource Grep
 * <p>
 * This utility extends the standard plain text grep to support archives, databases, web, images and other resources, 
 * to match a text pattern against resource names and content. 
 * </p>
 * <p>  
 * Two levels of matches are supports, the default 'listing' grep which behaves like a 'find -name <pattern>' and the 
 * more detailed 'content' grep which behaves like a file grep.
 * </p>
 * <p>
 * If <user.home>/.crgrep exists this properties file can be used to set global options. 
 * <table>
 * <tr><td><b>Property<b></td><td><b>Use</b></td></tr>
 * <tr><td>user</td><td>user name (for authentication)</td></tr>
 * <tr><td>password</td><td>password for user</td></tr>
 * <tr><td>uri</td><td>URI (jdbc URL etc)</td></tr>
 * <tr><td>maven.remoteRepos</td><td>List of maven remote repo URLs (semi-colon seperated)</td></tr>
 * <tr><td>maven.localRepo</td><td>Local maven remote if not $HOME/.m2/repository</td></tr>
 * <tr><td>maven.depth</td><td>Limit the POM depth of search</td></tr>
 * <tr><td>http.proxyHost</td><td>Proxy host if required by your company</td></tr>
 * <tr><td>http.proxyPort</td><td>Proxy port</td></tr>
 * </table>
 * </p>
 * <p>
 * Typical Usages:
 * </p>
 * <table>
 * <th>
 *      <td>Resource</td>
 *      <td>Description</td>
 *      <td>Example</td>
 * </th>
 * <tr>
 * <td>Database</td>
 * <td>
 *  	Search for matching table and column names and [optionally] column data.<br/>
 *  	The arguments '<pattern> <table-column> search for '<pattern>' matches against<br/>
 *      the <table-column> specifier with format 'TABLE-PATTERN[.COLUMN-PATTERN]'.<br/>
 *      
 * </td>
 * <td>
 *      crgrep -d -l "foo" "CUSTOMER_*.*"
 * </td>
 * </tr>
 *      
 * <tr>
 * <td>File</td>
 * <td>
 *  	Search for matching files and content.<br/>
 *  	Text files are simply checked for matches. Archive files (tar, gz, zip, jar, war, ear) are all treated as directories<br/>
 *  	and there listings are matched. For a content match the files within archives are extracted and their contents are<br/>
 *  	matched. Only non-binary files are extracted.<br/>
 * 		Image file usage:<br/>
 * 		Greps the meta-data within an image such as description, vendor data.<br/>
 *		Maven POM - determine dependency tree and grep the tree (jar files on the classpath) for class files etc.
 * </td>
 * <td>
 *      crgrep "foo" "src/*"
 * </td>
 * </tr>
 *
 * <tr>
 * <td>HTTP</td>
 * <td>
 * 		Given a URL of the form 'http://..' attempt to fetch the page (similar to wget) and grep the result for matches.
 * 		Recursive could mean follow href links to fetch referenced pages also (not implemented).
 * 
 * </td>
 * <td>
 *      crgrep "favicon" "http://www.google.com"
 * </td>
 * </tr>
 * </table>
 *
 * @author Craig Ryan
 */
public class ResourceGrep {

	private String user;
	private String pw;
	private String uri;
	private String proxy;
	private int proxyPort;
	private List<String> fileList;
	private String pattern;
	private AbstractGrep grep; 

    private static Options options = new Options();

    private Switches switches;

	/**
	 * Common Resource Grep.
	 * 
	 * @param args options followed by <pattern> <resources> (both support wildcards)
	 */
	public static void main(String[] args) {
		ResourceGrep rg = new ResourceGrep(args);
		rg.execute();
	}
	
	public ResourceGrep(String[] args) {
		if (!determineSearchCriteria(args)) {
			usage(null);
		}
		createGrep();
	}

	/**
	 * Execute the resource grep.
	 */
	public void execute() {
		if (grep != null) {
			grep.execute();
		}
	}

	/*
	 * Parse options and arguments.
	 */
	private boolean determineSearchCriteria(String[] args) {
		// create the command line parser
		CommandLineParser parser = new GnuParser();

		// create the Options
		options.addOption( "r", "recurse", false, "Recursive search into resource (directories, schema etc)" );
		// TODO: exclusions and inclusions to come, disable -e for now. 
		//options.addOption( "e", "exclude", false, "Exclude resource types. Comma sep. list. 'l' listing" );
		options.addOption( "l", "list", false, "List all matching resources by name but not their content" );
		options.addOption( "a", "allcontent", false, "All content data types (default is  non-numeric for db data)" );
		options.addOption( "d", "database", false, "Database grep (disables file search)" );
		options.addOption( "u", "user", true, "User (to access resource)" );
        options.addOption( "U", "uri", true, "URI (for db access)" );
        options.addOption( "m", "maven", false, "Add Maven POM file dependencies to resource list");
		options.addOption( "p", "password", true, "Password (to access resource)" );
		options.addOption( "P", "proxy", true, "Proxy settings (for http) as <host>[:<port>]" );
		options.addOption( "v", "verbose", false, "Verbose output (include warnings)" );
		options.addOption( "h", "help", false, "Help" );
		options.addOption( "X", "eXtensions", true, "Extensions comma sep. list. values: debug,trace." );
		CommandLine line;
		try {
//			int j = 0;
//			for (String arg : args) {
//				System.out.println("Enter search arg [" + j++ + "] = '" + arg + "'");
//			}
		    // parse the command line arguments
		    line = parser.parse( options, args );

		    switches = new Switches(line);
		    
		    if (line.hasOption('h')) {
		    	usage("Grep resources");
		    }
		    if (line.hasOption('u')) {
		    	user = line.getOptionValue('u');
		    }
		    if (line.hasOption('p')) {
		    	pw = line.getOptionValue('p');
		    }
		    if (line.hasOption('P')) {
		    	try {
		    		String px = line.getOptionValue('P');
		    		if (!px.startsWith("http:")) {
		    			px = "http://" + px;
		    		}
		    		URL pr = new URL(px);
		    		proxy = pr.getHost();
		    		proxyPort = pr.getPort();
		    		System.setProperty("http.proxyHost", proxy);
		    		System.setProperty("http.proxyPort", String.valueOf(proxyPort));
		    	} catch (MalformedURLException e) {
		    		usage("Invalid proxy URL syntax: " + e.getLocalizedMessage());
		    	}
		    }
		    if (line.hasOption('U')) {
		    	uri = line.getOptionValue('U');
		    }
		    if (line.hasOption('X')) {
		    	String ex = line.getOptionValue('X');
		    	switches.processExtensions(ex);
		    }
		}
		catch (ParseException exp) {
		    System.err.println( "Unexpected exception: " + exp.getMessage() );
			return false;
		} 
	
		loadUserProperties();
				
		String[] postOptionArgs = line.getArgs();
//			int i = 0;
//			for (String arg : postOptionArgs) {
//				System.out.println("Search arg [" + i++ + "] = '" + arg + "'");
//			}
//		}
		if (postOptionArgs == null || postOptionArgs.length < 1) {
			usage("Invalid arguments, expecting <Pattern> [<Resource(s)>], argument count was " + postOptionArgs.length);
		} else {
			// <pattern> [<resources>]
			pattern = postOptionArgs[0];
			
			if (postOptionArgs.length == 1) {
				fileList = Arrays.asList("*"); 
			} else {
				fileList = new ArrayList<String>(Arrays.asList(postOptionArgs));
				fileList.remove(0);
			}
		}
		
		checkDatabaseOptions();

		return true;
	}

	/*
	 * Check for ~/.crgrep for properties. These are only used if command line equivalents are not set (or available)
	 */
    private void loadUserProperties() {
    	String home = System.getProperty("user.home");
    	File dotFile = new File(home, ".crgrep");
    	if (!dotFile.exists() || !dotFile.isFile()) {
    		return;
    	}
    	Properties p = new Properties();
    	try {
//    		getDisplay().trace("Loading user preferences from file '" + dotFile.getPath());
			p.load(FileUtils.openInputStream(dotFile));
		} catch (IOException e) {
			return;
		}
    	switches.setUserProperties(p);
    	
    	// Command line settings take precedence
    	if (user == null) {
    		user = p.getProperty("user");
    	}
    	if (pw == null) {
    		pw = p.getProperty("password");
    	}
    	if (uri == null) {
    		uri = p.getProperty("uri");
    	}
    	if (proxy == null) {
    	    proxy = p.getProperty("http.proxyHost");
    	    if (proxy != null) {
    	        System.setProperty("http.proxyHost", proxy);
    	    }
    	}
    	if (proxyPort == 0) {
    	    String port = p.getProperty("http.proxyPort");
    	    try {
                proxyPort = Integer.parseInt(port); 
                System.setProperty("http.proxyPort", String.valueOf(proxyPort));
            } catch (NumberFormatException e) {
                // ignore
            }
    	}
    }

	// Check for valid DB options combination
	private void checkDatabaseOptions() {
		if (!switches.isDatabase()) {
			return;
		}
		if (uri == null) {
			if (fileList.size() > 1) {
				usage("Only one JDBC URL for database search is supported");
			}
			if (fileList.get(0).startsWith("jdbc:")) {
				uri = fileList.get(0);
			} else {
				usage("Missing JDBC URL for database search (-U / --uri)");
			}
		}
	}
	
	/*
	 * Establish a grep'er.
	 */
	private void createGrep() {
		ResourcePattern rp = new ResourcePattern(pattern);
		if (switches.isDatabase()) {
			grep = new DbGrep(switches);
			grep.setArguments(rp, new DbResourceList(fileList.get(0)));
		} else {
			// file or http
			if (fileList.get(0).startsWith("http:")) {
				if (fileList.size() > 1) {
					usage("HTTP search only supports a single URI argument (was given " + fileList.size() + ")");
				}
				grep = new HttpGrep(switches);
				grep.setArguments(rp, new FileResourceList(fileList));
			} else {
				grep = new FileGrep(switches);
				grep.setArguments(rp, new FileResourceList(fileList));
			}
		}
		grep.setUri(uri);
		grep.setUser(user);
		grep.setPw(pw);
		
		if (switches.isTrace()) {
			StringBuilder sb = new StringBuilder();
			for (String p : fileList) {
				sb.append(" ").append(p);
			}
			getDisplay().trace("Search pattern '" + pattern + "'");
			getDisplay().trace("Resource path(s) to search '" + sb.toString() + "'");
		}
	}
	
	public Display getDisplay() {
		if (grep == null) {
			return null;
		}
		return grep.getDisplay();
	}

	public void setDisplay(Display display) {
		if (grep != null) {
			grep.setDisplay(display);
		}
	}

	private static void usage(String msg) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("[options] <search-pattern> <resources-pattern>", msg, options, null, true);
		System.exit(-1);
	}

	public Switches getSwitches() {
		return switches;
	}
}
