@echo off
rem
rem Crgrep start script.
rem
rem Author: Craig Ryan [cryan.dublin@gmail.com]
rem
rem Environment Variables
rem
rem   CRGREP_HOME       (Optional) Home directory for crgrep
rem   CRGREP_OPTS       (Optional) Java runtime options used when the command is
rem                      executed.
rem
rem   CRGREP_TMPDIR     (Optional) Directory path location of temporary directory
rem                      the JVM should use (java.io.tmpdir).  Defaults to
rem                      $CRGREP_BASE/temp.
rem
rem   JAVA_HOME          Must point at your Java Development Kit installation.
rem
rem   CRGREP_JVM_OPTS   (Optional) Java runtime options used when the command is
rem                       executed.
rem
rem -----------------------------------------------------------------------------

rem
rem Determine if JAVA_HOME is set and if so then use it
rem
if not "%JAVA_HOME%"=="" goto found_java

set CRGREP_JAVACMD=java
goto file_locate

:found_java
set CRGREP_JAVACMD=%JAVA_HOME%\bin\java

:file_locate

rem
rem Locate where crgrep is in filesystem
rem
if not "%OS%"=="Windows_NT" goto start

rem %~dp0 is name of current script under NT
set CRGREP_HOME=%~dp0*

rem : operator works similar to make : operator
set CRGREP_HOME=%CRGREP_HOME:\bin\*=%

:start

if not "%CRGREP_HOME%" == "" goto crgrep_home

rem echo.
rem echo Error: CRGREP_HOME environment variable is not set.
rem echo   This needs to be set manually for Win9x as its command
rem echo   prompt scripting does not allow it to be set automatically.
rem echo.
rem goto end

set CRGREP_HOME=.

:crgrep_home

if not "%CRGREP_TMPDIR%"=="" goto afterTmpDir
set CRGREP_TMPDIR=%CRGREP_HOME%\temp
if not exist "%CRGREP_TMPDIR%" mkdir "%CRGREP_TMPDIR%"

:afterTmpDir

rem echo Using CRGREP_HOME:   %CRGREP_HOME%
rem echo Using CRGREP_TMPDIR: %CRGREP_TMPDIR%
rem echo Using JAVA_HOME:      %JAVA_HOME%

rem set CRGREP_SM=
rem if "%CRGREP_SECURE%" == "false" goto postSecure
rem Make Crgrep run with security Manager enabled
rem set CRGREP_SM="-Djava.security.manager"
rem :postSecure

rem Crgrep system properties
rem set CRGREP_JVM_OPTS=%CRGREP_JVM_OPTS% -Dblah

rem uncomment to enable remote debugging 
rem set DEBUG=-Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=y

"%CRGREP_JAVACMD%" %DEBUG% "-Dcrgrep.home=%CRGREP_HOME%" %CRGREP_JVM_OPTS% -Djava.ext.dirs="%CRGREP_HOME%\lib" com.ryanfuse.crgrep.ResourceGrep %1 %2 %3 %4 %5 %6 %7 %8 %9

:end

