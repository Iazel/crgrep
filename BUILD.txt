CRGREP (C) Copyright 2013 Craig Ryan.

Build and Install Instructions

For building CRGREP from a source distribution

System Requirements
-------------------

	Native Windows, Cygwin and Un*x platforms
	Java 1.6+

To install CRGREP follow these steps
----------------------------------------------

1. From the top directory run the maven package target (skipping tests is optional)

   $ cd <crgrep-dir>
   $ mvn clean
   $ mvn package -DskipTests

2. Unzip the resulting distribution into your favourite software folder

   $ cd <software>
   $ unzip <crgrep-dir>/distribution/target/crgrep-<version>.zip

3. Update your PATH

   PATH=...;<software>/crgrep-<version>/bin

   Also ensure you have Java (1.6+) installed and JAVA_HOME set in your environment, for example:

   JAVA_HOME=C:\Program Files\Java\jdk1.6.0_32

4. Execute crgrep from the command line

   Depending on the unzip behaviour on your system you may need to set
   the extracted scripts to be executable. (Unix example:)

   $ chmod 755 <software>/crgrep-<version>/bin/crgrep*

   Start using it.

   $ crgrep Stuff mydir/*


To build and test CRGREP some tips and guides
----------------------------------------------

The project builds its own test archive files used by its test suites,
it also builds a distribution ZIP as described above. 

From the top directory to build CRGREP and run the unit tests

   $ mvn clean
   $ mvn package

Often seeing a strange Access denied error in test-war if run as 'mvn clean package'. Need to run
'mvn clean' on its own in this case.

If this fails tests, may need to run 'mvn package -DskipTests' first, then 'mvn clean ' followed by 'mvn test' 
so that generated test archives are in the right place when the tests run.

Integration tests are kept under a seperate com.ryanfuse.crgrep.integration package and require environment
specific custom settings to get them to pass.

It is advisable to firstly create a custom settings file under HOME rather than specifying lengthy command
line options such as JDBC connection data. 

The README.txt outlines how to setup HOME/.crgrep for this purpose. A template of the file can be
found in bin/dot-crgrep.

To run Integration tests its easiest within Eclipse or similar IDE. Select a test from the 
com.ryanfuse.crgrep.integration package and select 'Run As -> Junit Test'.


