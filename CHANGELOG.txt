CRGREP - Common Resource Grep

Change log and roadmap.

ROADMAP
=============================================================

Future wish-list.

- SevenZArchiveEntry support for .7z files. Requires commons-compress 1.6 which is not yet released.
- Other archive formats: bzip, ar, cpio, dump etc
- Database column data containing URLs, option to follow these links.
- Follow links in html pages (recursive)
- More grep features, -i (ignore case), -x (whole line match only), -s (no errors)
- Some Find features (time based options, -type, -user)
- CRGREP_OPTIONS env var (like GREP_OPTIONS) for default options
- inclusion/exclusion filters for resource lists
- search MS documents: PPT, XLS etc (apache POI/Tika)
- search text within images with the use of OCR tools
  (possible solutions: http://stackoverflow.com/questions/1813881/java-ocr-implementation)
- formatted output: html, reports
- read from a file containing resource paths
- options to output to file or stream (for use as a library)
- support for encrypted files (bouncycastle)

Issues to fix:
- doesn't disable pom related logging:  [main] INFO com.ning.http.client.providers.netty.NettyAsyncHttpProvider
- core/src/test/resources build from src?
- fix tests to not use versions in test archives (0.1 etc)

CHANGE LOG
=============================================================

0.3 (20-Oct-2013)
-----------------
New features:
	- Search text extracted from PDF Files
	- New '-' resource list argument to read from standard input
Bugs and issues fixes:
	- warn of file/directory paths which don't exist
	- DB search now checks for only one resource path argument
Known issues:
	- PDF search output reports page and line number. The line number does not consider whitespace only lines
	  so the linenum is a guide only.
Documentation updates:
	- CHANGELOG.txt and README.txt updated

0.2 (15-Aug-2013)
-----------------
New features:
	- Supports multiple resource paths on the command line (for file grep only, for DB/HTTP search a single jdbc 
	  resource argument is supported)
Bug fixes:
	- un*x main bash script 'crgrep' failed when command line argument expansion occurred. Required that crgrep be 
	  updated to support multiple resource paths.
	- JAVA_HOME path with spaces (Windows) caused un*x 'crgrep' script to fail with unknown path error.
Known issues:
    - CYWGIN support varies as noted in the README.txt
Documentation updates: 
	- README.txt updated

0.1 (28-Jul-2013)
-----------------
First public release.

New features:
	- Initial implementation
	- Supports plain files, archives (tar, zip, jar, ear, war, tar.gz), images, maven pom dependencies, http
	- Database support for H2, MySQL, Oracle, Sqlite
Bug fixes:
Documentation updates: 

